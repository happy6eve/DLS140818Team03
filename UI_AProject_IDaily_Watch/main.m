//
//  main.m
//  UI_AProject_IDaily_Watch
//
//  Created by 徐继垚 on 14/10/22.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
