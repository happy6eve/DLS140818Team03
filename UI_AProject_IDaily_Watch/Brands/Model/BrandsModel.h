//
//  MyModel.h
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-14.
//  Copyright (c) 2014年 蓝欧科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BrandsModel : NSObject
@property(nonatomic,retain)NSString *image1;
@property(nonatomic,retain)NSString *image2;
@property(nonatomic,retain)NSString *title;
@property(nonatomic,retain)NSString *author;
@property(nonatomic,retain)NSString *summary;
@property(nonatomic,retain)NSString *pubdate1;
@property(nonatomic,retain)NSString *content;
@end
