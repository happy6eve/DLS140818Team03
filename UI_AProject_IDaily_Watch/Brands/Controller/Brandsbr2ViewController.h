//
//  Brandsbr2ViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-15.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Brandsbr2ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property(nonatomic,retain)UITableView *table5;
@property(nonatomic,retain)UIWebView *webView1;
@property(nonatomic,retain)NSArray *arr1;
@property(nonatomic,retain)NSString *number;
@property(nonatomic,retain)NSMutableArray *array4;
@property(nonatomic,retain)NSString *str;
@property(nonatomic,retain)NSString *str1;
@property(nonatomic,retain)NSString *str2;
@property(nonatomic,retain)NSString *str3;
@property(nonatomic,retain)NSString *getNum;
@property(nonatomic,retain)NSMutableArray *array6;
@property(nonatomic,retain)NSMutableArray *arrNew;
@property(nonatomic,retain)NSMutableArray *arrNew1;
@property(nonatomic,retain)NSString *str4;
@property(nonatomic,retain)UIAlertView *alert;
@property(nonatomic,assign)NSInteger num;
//@property(nonatomic,strong)NSRange bigrange;
@end
