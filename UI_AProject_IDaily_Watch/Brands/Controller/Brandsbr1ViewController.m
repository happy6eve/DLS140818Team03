//
//  Brandsbr1ViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-15.
//  Copyright (c) 2014年 蓝欧科技. All rights reserved.
//

#import "Brandsbr1ViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "BrandsModel.h"
#import "UIButton+WebCache.h"

#import "Brandsbr2ViewController.h"

@interface Brandsbr1ViewController ()

@end

@implementation Brandsbr1ViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _array3 = [[NSMutableArray alloc]init];
        _dic = [[NSMutableDictionary alloc]init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
        // Do any additional setup after loading the view.
    
//    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    //self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.translucent = NO;
//    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(Width * 80.0f, 0.0f, Width * 160.0f, Height * 44.0f)];
//    imageView.image = [UIImage imageNamed:@"LOGO1.png"];
//    [self.navigationController.navigationBar addSubview:imageView];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"6.jpeg"]]];
    _scrolls = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * (568 ))];
    [_scrolls setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"6.jpeg"]]];
    _scrolls.contentSize = CGSizeMake(Width * 320, Height * (568 ) * 1.6);
    [self.view addSubview:_scrolls];

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, Width * 60, Height * 30);
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回1.png"] forState:UIControlStateNormal];
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回2.png"] forState:UIControlStateHighlighted];
    [button setTitle:@"返回" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button setTintColor:[UIColor whiteColor]];
    [button addTarget:self action:@selector(buttonActionBrand) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = item;
    
    
    AFHTTPRequestOperationManager *manager1 = [AFHTTPRequestOperationManager manager];
    [manager1 GET:@"http://watch-cdn.idailywatch.com/api/list/brands/zh-hans?page=1&ver=iphone&app_ver=9" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *arr1 = (NSArray *)responseObject;
        
        for (NSDictionary *dic1 in arr1) {
            BrandsModel *mine2 = [[BrandsModel alloc]init];
            mine2.image1 = [dic1 objectForKey:@"cover_landscape_hd"];
            mine2.title = [dic1 objectForKey:@"title"];
            mine2.pubdate1 = [dic1 objectForKey:@"pubdate"];
            mine2.summary = [dic1 objectForKey:@"summary"];
            mine2.content = [dic1 objectForKey:@"content"];
            [_array3 addObject:mine2];
            
            
        }
        
            
        for (unichar i = 'A'; i <= 'Z'; i++) {
            self.arra1 = [NSMutableArray array];
            
            for (int j = 0; j < _array3.count; j ++) {
                if ([[[_array3[j]title]substringToIndex:1] isEqualToString:[NSString stringWithFormat:@"%c",i]]) {
                    BrandsModel *brands = [[BrandsModel alloc]init];
                    brands.image1 = [NSString stringWithFormat:@"%@",[_array3[j]image1]];
                    brands.title = [NSString stringWithFormat:@"%@",[_array3[j]title]];
                    brands.summary = [NSString stringWithFormat:@"%@",[_array3[j]summary]];
                    brands.content = [NSString stringWithFormat:@"%@",[_array3[j]content]];
                    brands.pubdate1 = [NSString stringWithFormat:@"%@",[_array3[j]pubdate1]];
//                    NSString *str2 = [NSString stringWithFormat:@"%@",[_array3[j]image1]];
                    
                    
                    [_arra1 addObject:brands];
                    
                }
            }
            [_dic setObject:_arra1 forKey:[NSString stringWithFormat:@"%c",i]];
            [self reloadInputViews];
            
            

        }
//        NSLog(@"%@",_dic);
        for (unichar j = 'A'; j < 'Z'; j ++) {
            
        
        if ([_getcontent isEqualToString:[NSString stringWithFormat:@"%c",j]]) {
            
            _arr = (NSArray *)[_dic objectForKey:[NSString stringWithFormat:@"%c",j]];
            //_arrWc = [ _arr[j]image1];
//            NSLog(@"🍎🍎🍎🍎🍎🍎🍎🍎🍎🍎🍎%lu",(unsigned long)_arr.count);
            for (int i = 0 ; i < _arr.count ; i ++) {
                _aView3 = [[UIView alloc]initWithFrame:CGRectMake(Width * 18, Height * (18 + i * 100), Width * 207, Height * 90)];
                _aView3.layer.cornerRadius = 10;
                _aView3.layer.masksToBounds = YES;
                [_aView3 setBackgroundColor:[UIColor lightGrayColor]];
                
                _cbutton2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                _cbutton2.frame = CGRectMake(0, Height * 5, Width * 205, Height * 80);
                _cbutton2.backgroundColor = [UIColor blackColor];
                _cbutton2.layer.cornerRadius = 10;
                _cbutton2.layer.shadowColor = [UIColor whiteColor].CGColor;
                _cbutton2.layer.shadowOpacity = 1;
                
                CATransform3D transform = CATransform3DIdentity;
                transform.m34 = 0.00005;
                transform = CATransform3DRotate( transform,( M_PI/180 * 20), 0,1,0);
                [_cbutton2.layer setTransform:transform];
                _cbutton2.layer.masksToBounds = YES;
                _cbutton2.tag = 100 + i;
                if (_cbutton2.tag == 100) {
                    [_cbutton2 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
                } else if(_cbutton2.tag == 101)
                {
                [_cbutton2 addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
                
                }else if(_cbutton2.tag == 102)
                {
                    [_cbutton2 addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
                    
                }else if(_cbutton2.tag == 103)
                {
                    [_cbutton2 addTarget:self action:@selector(button3Action:) forControlEvents:UIControlEventTouchUpInside];
                    
                }else if(_cbutton2.tag == 104)
                {
                    [_cbutton2 addTarget:self action:@selector(button4Action:) forControlEvents:UIControlEventTouchUpInside];
                    
                }else if(_cbutton2.tag == 105)
                {
                    [_cbutton2 addTarget:self action:@selector(button5Action:) forControlEvents:UIControlEventTouchUpInside];
                    
                }else if(_cbutton2.tag == 106)
                {
                    [_cbutton2 addTarget:self action:@selector(button6Action:) forControlEvents:UIControlEventTouchUpInside];
                    
                }else if(_cbutton2.tag == 107)
                {
                    [_cbutton2 addTarget:self action:@selector(button7Action:) forControlEvents:UIControlEventTouchUpInside];
                    
                }
                
                [_cbutton2 setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[_arr[i] image1]]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"1.png"]];
//                NSLog(@"%@",[_arr[i] image1]);
                
                
                [_scrolls addSubview:_aView3];
                [_aView3 addSubview:_cbutton2];
            }
            
            
            
        }
            

        }
       
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    

}
- (void)buttonActionBrand
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)buttonAction:(UIButton *)button
{
    
   
    
    Brandsbr2ViewController *vc = [[Brandsbr2ViewController alloc]init];
    vc.arr1 = _arr;
    vc.array4 = _array3;
   // NSLog(@"😊%@",[_array3[(long)button.tag - 100] title]);
    vc.number = [NSString stringWithFormat:@"%lu",(long)button.tag];
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(void)button1Action:(UIButton *)button
{
    
   
    
    Brandsbr2ViewController *vc = [[Brandsbr2ViewController alloc]init];
    vc.arr1 = _arr;
    vc.array4 = _array3;
    //NSLog(@"😊%@",[_array3[(long)button.tag - 100] title]);
    vc.number = [NSString stringWithFormat:@"%lu",(long)button.tag];
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(void)button2Action:(UIButton *)button
{
    
    
    
    Brandsbr2ViewController *vc = [[Brandsbr2ViewController alloc]init];
    vc.arr1 = _arr;
    vc.array4 = _array3;
    //NSLog(@"😊%@",[_array3[(long)button.tag - 100] title]);
    vc.number = [NSString stringWithFormat:@"%lu",(long)button.tag];
    [self.navigationController pushViewController:vc animated:YES];
   
    
}
-(void)button3Action:(UIButton *)button
{
    
    
    
    Brandsbr2ViewController *vc = [[Brandsbr2ViewController alloc]init];
    vc.arr1 = _arr;
    vc.array4 = _array3;
   // NSLog(@"😊%@",[_array3[(long)button.tag - 100] title]);
    vc.number = [NSString stringWithFormat:@"%lu",(long)button.tag];
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(void)button4Action:(UIButton *)button
{
    
    
    
    Brandsbr2ViewController *vc = [[Brandsbr2ViewController alloc]init];
    vc.arr1 = _arr;
    vc.array4 = _array3;
   // NSLog(@"😊%@",[_array3[(long)button.tag - 100] title]);
    vc.number = [NSString stringWithFormat:@"%lu",(long)button.tag];
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(void)button5Action:(UIButton *)button
{
    
    
    
    Brandsbr2ViewController *vc = [[Brandsbr2ViewController alloc]init];
    vc.arr1 = _arr;
    vc.array4 = _array3;
    //NSLog(@"😊%@",[_array3[(long)button.tag - 100] title]);
    vc.number = [NSString stringWithFormat:@"%lu",(long)button.tag];
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(void)button6Action:(UIButton *)button
{
    
   
    
    Brandsbr2ViewController *vc = [[Brandsbr2ViewController alloc]init];
    vc.arr1 = _arr;
    vc.array4 = _array3;
    //NSLog(@"😊%@",[_array3[(long)button.tag - 100] title]);
    vc.number = [NSString stringWithFormat:@"%lu",(long)button.tag];
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(void)button7Action:(UIButton *)button
{
    
    
    
    Brandsbr2ViewController *vc = [[Brandsbr2ViewController alloc]init];
    vc.arr1 = _arr;
    vc.array4 = _array3;
    //NSLog(@"😊%@",[_array3[(long)button.tag - 100] title]);
    vc.number = [NSString stringWithFormat:@"%lu",(long)button.tag];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
