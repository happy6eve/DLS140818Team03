//
//  BrandsViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-14.
//  Copyright (c) 2014年 蓝欧科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrandsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,UIActionSheetDelegate,UISearchBarDelegate>
@property(nonatomic,retain)UIScrollView *scroll1;
@property(nonatomic,retain)UIButton *button1;
@property(nonatomic,retain)UIButton *button2;
@property(nonatomic,retain)UIButton *button3;
@property(nonatomic,retain)UIButton *button4;
@property(nonatomic,retain)UIButton *button5;
@property(nonatomic,retain)UIButton *button6;
@property(nonatomic,retain)UIButton *button7;
@property(nonatomic,retain)UIButton *button8;
@property(nonatomic,retain)UIButton *button9;
@property(nonatomic,retain)UIButton *button10;
@property(nonatomic,retain)UIButton *button11;
@property(nonatomic,retain)UIButton *button12;
@property(nonatomic,retain)UIButton *button13;
@property(nonatomic,retain)UIButton *button14;
@property(nonatomic,retain)UIButton *button15;
@property(nonatomic,retain)UIButton *button16;
@property(nonatomic,retain)UIButton *button17;
@property(nonatomic,retain)UIButton *button18;
@property(nonatomic,retain)UIButton *button19;
@property(nonatomic,retain)UIButton *button20;
@property(nonatomic,retain)UIButton *button21;
@property(nonatomic,retain)UIButton *button22;
@property(nonatomic,retain)UIButton *button23;
@property(nonatomic,retain)UIButton *button24;
@property(nonatomic,retain)UIButton *button25;
@property(nonatomic,retain)UIButton *button26;
@property(nonatomic,retain)UIView *aView;
@property(nonatomic,retain)NSMutableArray *array2;
@property(nonatomic,retain)NSString *buttonContent;
@property (retain, nonatomic) IBOutlet UIImageView *weathImage;
@property (retain, nonatomic) IBOutlet UILabel *weathText;
@property(nonatomic,retain)UIView *aView2;
@property(nonatomic,retain)UIButton *buttonContent1;
@property(nonatomic,retain)UILabel *lableweather1;
@property(nonatomic,retain)UILabel *lableweather2;
@property(nonatomic,retain)UILabel *lableweather3;
@property(nonatomic,retain)UISearchBar *searchBar;
@property(nonatomic,retain)NSMutableArray *array5;
@property(nonatomic,retain)NSMutableArray *array5New;
@property(nonatomic,retain)NSMutableArray *arra2;
@property(nonatomic,retain)NSDictionary *dic1;
@property(nonatomic,retain)NSMutableArray *arraya6;
@property(nonatomic,retain)NSMutableArray *arrayamiddle;
@property(nonatomic,retain)NSMutableArray *arraya7;
@property(nonatomic,retain)NSMutableArray *arraya8;
@property(nonatomic,retain)UITableView *tableB;
@property(nonatomic,retain)UITableView *tableC;
@property(nonatomic,retain)UIBarButtonItem *buttonItem1;
@property(nonatomic,retain)UITableViewCell *cell;
@property(nonatomic,retain)UIButton *buttonSelect;
@property(nonatomic,retain)NSArray *backgroundArray;
@property(nonatomic,retain)UILabel *labled1;
@end
