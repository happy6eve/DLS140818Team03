//
//  BrandsViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-14.
//  Copyright (c) 2014年 蓝欧科技. All rights reserved.
//

#import "BrandsViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "BrandsModel.h"
#import "Brandsbr1ViewController.h"
#import "Brandsbr2ViewController.h"

@interface BrandsViewController ()
@property(nonatomic,retain)UIImage *pic;
@end

@implementation BrandsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.array2 = [NSMutableArray array];
        self.array5 = [NSMutableArray array];
        self.array5New = [[NSMutableArray alloc]init];
        self.arraya6 = [NSMutableArray array];
        self.arraya7 = [NSMutableArray array];
        self.arraya8 = [NSMutableArray array];
        self.arrayamiddle = [NSMutableArray array];
        self.backgroundArray = [NSArray arrayWithObjects:@"经典黑",@"清新粉",@"宝石蓝",@"古典",@"唯美",@"豪华",@"非主流",@"风景",@"人物", nil];
            }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //self.navigationController.navigationBar.hidden = YES;
    
    
//    [super viewDidLoad];
//    self.pic = [UIImage imageNamed:@"snow.png"];//初始化图片
//    //启动定时器，实现飘雪效果
//    [NSTimer scheduledTimerWithTimeInterval:(0.2) target:self selector:@selector(ontime) userInfo:nil repeats:YES];
    
    
    [self getArray2];
    //tableView 创建搜索提示框
    
    
//    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
//    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(Width * 80.0f, 0.0f, Width * 160.0f, Height * 44.0f)];
//    imageView.image = [UIImage imageNamed:@"LOGO1.png"];
//    [self.navigationController.navigationBar addSubview:imageView];
    
    //左上角button
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.frame = CGRectMake(0, 0, Width * 30, Height * 30);
    [button1 setBackgroundImage:[UIImage imageNamed:@"TodayHome2.png"] forState:UIControlStateNormal];
    [button1 setBackgroundImage:[UIImage imageNamed:@"TodayHome1.png"] forState:UIControlStateHighlighted];
    [button1 addTarget:self action:@selector(buttonReturnHome) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc]initWithCustomView:button1];
    self.navigationItem.leftBarButtonItem = item1;
    //右上角搜索
    _buttonItem1 = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchAction:)];
    _buttonItem1.accessibilityViewIsModal = YES;
    self.navigationItem.rightBarButtonItem = _buttonItem1;
    
    
    
    
    
    _scroll1 = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,Width * 320, Height * (568 - 64))];
    _scroll1.delegate = self;
    _scroll1.contentSize = CGSizeMake(Width * 320 * 2,Height * (568 - 64));
    //_scroll1.backgroundColor = [UIColor orangeColor];
    [_scroll1 setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"779.jpg"]]];
    
    
    [self.view addSubview:_scroll1];
    
    _buttonSelect = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _buttonSelect.frame = CGRectMake(Width * 10, Height *70, Width *80, Height *30);
    [_buttonSelect setTitle:@"主题背景" forState:UIControlStateNormal];
    _buttonSelect.backgroundColor = [UIColor whiteColor];
    _buttonSelect.alpha = 0.5;
    [_buttonSelect addTarget:self action:@selector(buttonSelect:) forControlEvents:UIControlEventTouchUpInside];
    _buttonSelect.layer.cornerRadius = 10;
    _buttonSelect.layer.masksToBounds = YES;
    [_scroll1 addSubview:_buttonSelect];
    
    _labled1 = [[UILabel alloc]initWithFrame:CGRectMake(0, Height * 435, Width * 320, Height * 50)];
    _labled1.backgroundColor = [UIColor blackColor];
    _labled1.alpha = 1;
    _labled1.text = @"请选择商标首字母";
    _labled1.font = [UIFont systemFontOfSize:14];
    _labled1.textColor = [UIColor whiteColor];
    _labled1.textAlignment = NSTextAlignmentCenter;
    _labled1.layer.cornerRadius = 10;
    _labled1.layer.masksToBounds = YES;
    [self.view addSubview:_labled1];
    
    _aView = [[UIView alloc]initWithFrame:CGRectMake(Width * 80, Height * 35, Width * 520, Height * 345)];
    
    _aView.backgroundColor = [UIColor clearColor];
    _aView.layer.transform = CATransform3DMakeRotation(0, 0, 0, 0);
    [_scroll1 addSubview:_aView];
    _button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button1.alpha = 0.4;
    _button1.frame = CGRectMake(Width * 60 , Height * 35, Width * 100, Height * 100);
    [_button1 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_button1 setTitle:@"A" forState:UIControlStateNormal];
    _button1.backgroundColor = [UIColor whiteColor];
    _button1.tintColor = [UIColor blueColor];
    [_button1.titleLabel setFont:[UIFont systemFontOfSize:30]];
    [_aView addSubview:_button1];
    
    _button2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button2.alpha = 0.4;
    _button2.frame = CGRectMake(Width * 165, Height * 35, Width * 45, Height * 45);
    [_button2 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_button2 setTitle:@"B" forState:UIControlStateNormal];
    _button2.tintColor = [UIColor magentaColor];
    [_button2.titleLabel setFont:[UIFont systemFontOfSize:12]];
    _button2.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button2];
    
    _button3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button3.alpha = 0.4;
    _button3.frame = CGRectMake(Width * 215, Height * 35, Width * 45, Height * 45);
    [_button3 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_button3 setTitle:@"C" forState:UIControlStateNormal];
    _button3.tintColor = [UIColor redColor];
    [_button3.titleLabel setFont:[UIFont systemFontOfSize:12]];
    _button3.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button3];
    
    _button4 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button4.alpha = 0.4;
    _button4.frame = CGRectMake(Width * 165, Height * 85, Width * 45, Height * 45);
    [_button4 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button4.tintColor = [UIColor grayColor];
    [_button4.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_button4 setTitle:@"D" forState:UIControlStateNormal];
    _button4.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button4];
    
    _button5 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button5.alpha = 0.4;
    _button5.frame = CGRectMake(Width * 215, Height * 85, Width * 45, Height * 45);
    [_button5 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button5.tintColor = [UIColor redColor];
    [_button5.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_button5 setTitle:@"E" forState:UIControlStateNormal];
    _button5.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button5];
    
    _button6 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button6.alpha = 0.4;
    _button6.frame = CGRectMake(Width * 265, Height * 35, Width * 200, Height * 100);
    [_button6 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button6.tintColor = [UIColor greenColor];

    [_button6.titleLabel setFont:[UIFont systemFontOfSize:60]];
    [_button6 setTitle:@"F" forState:UIControlStateNormal];
    _button6.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button6];
    
    _button7 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button7.alpha = 0.4;
    _button7.frame = CGRectMake(Width * 470, Height * 35, Width * 100, Height * 100);
    [_button7 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button7.tintColor = [UIColor redColor];
    [_button7.titleLabel setFont:[UIFont systemFontOfSize:30]];
    [_button7 setTitle:@"G" forState:UIControlStateNormal];
    _button7.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button7];
    
    _button8 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button8.alpha = 0.4;
    _button8.frame = CGRectMake(Width * 60, Height * 140, Width * 200, Height * 100);
    [_button8 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button8.tintColor = [UIColor redColor];
    [_button8.titleLabel setFont:[UIFont systemFontOfSize:60]];
    [_button8 setTitle:@"H" forState:UIControlStateNormal];
    _button8.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button8];
    
    _button9 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button9.alpha = 0.4;
    _button9.frame = CGRectMake(Width * 265, Height * 140, Width * 45, Height * 100);
    [_button9 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button9.tintColor = [UIColor redColor];
    [_button9.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_button9 setTitle:@"I" forState:UIControlStateNormal];
    _button9.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button9];
    
    _button10 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button10.alpha = 0.4;
    _button10.frame = CGRectMake(Width * 315, Height * 140, Width * 45, Height * 100);
    [_button10 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button10.tintColor = [UIColor blueColor];
    [_button10.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_button10 setTitle:@"J" forState:UIControlStateNormal];
    _button10.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button10];
    
    _button11 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button11.alpha = 0.4;
    _button11.frame = CGRectMake(Width * 365, Height * 140, Width * 45, Height * 45);
    [_button11 addTarget:self action:@selector(buttonActionNo:) forControlEvents:UIControlEventTouchUpInside];
    _button11.tintColor = [UIColor magentaColor];
    [_button11.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_button11 setTitle:@"K" forState:UIControlStateNormal];
    _button11.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button11];
    
    _button12 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button12.alpha = 0.4;
    _button12.frame = CGRectMake(415, 140, 45, 45);
    [_button12 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button12.tintColor = [UIColor greenColor];
    [_button12.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_button12 setTitle:@"L" forState:UIControlStateNormal];
    _button12.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button12];
    
    _button13 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button13.alpha = 0.4;
    _button13.frame = CGRectMake(Width * 365, Height * 190, Width * 45, Height * 45);
    [_button13 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button13.tintColor = [UIColor redColor];
    [_button13.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_button13 setTitle:@"M" forState:UIControlStateNormal];
    _button13.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button13];
    
    _button14 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button14.alpha = 0.4;
    _button14.frame = CGRectMake(Width * 415, Height * 190, Width * 45, Height * 45);
    [_button14 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button14.tintColor = [UIColor blueColor];
    [_button14.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_button14 setTitle:@"N" forState:UIControlStateNormal];
    _button14.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button14];
    
    _button15 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button15.alpha = 0.4;
    _button15.frame = CGRectMake(Width * 470, Height * 140, Width * 100, Height * 100);
    [_button15 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button15.tintColor = [UIColor redColor];
    [_button15.titleLabel setFont:[UIFont systemFontOfSize:30]];
    [_button15 setTitle:@"O" forState:UIControlStateNormal];
    _button15.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button15];
    
    _button16 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button16.alpha = 0.4;
    _button16.frame = CGRectMake(Width * 60, Height * 250, Width * 45, Height * 45);
    [_button16 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button16.tintColor = [UIColor redColor];
    [_button16.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_button16 setTitle:@"P" forState:UIControlStateNormal];
    _button16.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button16];
    
    _button17 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button17.alpha = 0.4;
    _button17.frame = CGRectMake(Width * 110, Height * 250, Width * 45, Height * 45);
    [_button17 addTarget:self action:@selector(buttonActionNo:) forControlEvents:UIControlEventTouchUpInside];
    _button17.tintColor = [UIColor blueColor];
    [_button17.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_button17 setTitle:@"Q" forState:UIControlStateNormal];
    _button17.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button17];

    
    _button18 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button18.alpha = 0.4;
    _button18.frame = CGRectMake(Width * 60, Height * 305, Width * 45, Height * 45);
    [_button18 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button18.tintColor = [UIColor redColor];
    [_button18.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_button18 setTitle:@"R" forState:UIControlStateNormal];
    _button18.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button18];
    
    _button19 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button19.alpha = 0.4;
    _button19.frame = CGRectMake(Width * 110, Height * 305, Width * 45, Height * 45);
    [_button19 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button19.tintColor = [UIColor redColor];
    [_button19.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_button19 setTitle:@"S" forState:UIControlStateNormal];
    _button19.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button19];

    
    _button20 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button20.alpha = 0.4;
    _button20.frame = CGRectMake(Width * 160, Height * 250, Width * 100, Height * 100);
    [_button20 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button20.tintColor = [UIColor redColor];
    [_button20.titleLabel setFont:[UIFont systemFontOfSize:30]];
    [_button20 setTitle:@"T" forState:UIControlStateNormal];
    _button20.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button20];
    
    _button21 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button21.alpha = 0.4;
    _button21.frame = CGRectMake(Width * 265, Height * 250, Width * 100, Height * 45);
    [_button21 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button21.tintColor = [UIColor greenColor];
    [_button21.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [_button21 setTitle:@"U" forState:UIControlStateNormal];
    _button21.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button21];
    
    _button22 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button22.alpha = 0.4;
    _button22.frame = CGRectMake(Width * 265, Height * 305, Width * 100, Height * 45);
    [_button22 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _button22.tintColor = [UIColor blueColor];
    [_button22.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [_button22 setTitle:@"V" forState:UIControlStateNormal];
    _button22.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button22];
    
    _button23 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button23.alpha = 0.4;
    _button23.frame = CGRectMake(Width * 370, Height * 250, Width * 45, Height * 100);
    [_button23 addTarget:self action:@selector(buttonActionNo:) forControlEvents:UIControlEventTouchUpInside];
    _button23.tintColor = [UIColor redColor];
    [_button23.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_button23 setTitle:@"W" forState:UIControlStateNormal];
    _button23.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button23];
    
    _button24 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button24.alpha = 0.4;
    _button24.frame = CGRectMake(Width * 420, Height * 250, Width * 45, Height * 100);
    [_button24 addTarget:self action:@selector(buttonActionNo:) forControlEvents:UIControlEventTouchUpInside];
    _button24.tintColor = [UIColor redColor];
    [_button24.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [_button24 setTitle:@"X" forState:UIControlStateNormal];
    _button24.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button24];
    
    
    _button25 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button25.alpha = 0.4;
    _button25.frame = CGRectMake(Width * 470, Height * 250, Width * 100, Height * 45);
    [_button25 addTarget:self action:@selector(buttonActionNo:) forControlEvents:UIControlEventTouchUpInside];
    _button25.tintColor = [UIColor redColor];
    [_button25.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [_button25 setTitle:@"Y" forState:UIControlStateNormal];
    _button25.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button25];
    
    _button26 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button26.alpha = 0.4;
    _button26.frame = CGRectMake(Width * 470, Height * 305, Width * 100, Height * 45);
    [_button26 addTarget:self action:@selector(buttonActionNo:) forControlEvents:UIControlEventTouchUpInside];
    _button26.tintColor = [UIColor greenColor];
    [_button26.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [_button26 setTitle:@"Z" forState:UIControlStateNormal];
    _button26.backgroundColor = [UIColor whiteColor];
    [_aView addSubview:_button26];
    
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * 30)];
    _searchBar.showsSearchResultsButton = YES;
    _searchBar.barTintColor = [UIColor blackColor];
    _searchBar.searchResultsButtonSelected = YES;
    _searchBar.placeholder = @"请输入要搜索的品牌名";
    _searchBar.delegate = self;
    _searchBar.keyboardType = UIKeyboardTypeDefault;
    
    [self.view addSubview:_searchBar];
    
    
    _tableB = [[UITableView alloc]initWithFrame:CGRectMake(0, Height * -40, 0, 0) style:UITableViewStylePlain];
    _tableB.layer.cornerRadius = 10;
    _tableB.layer.masksToBounds = YES;
    _tableB.delegate = self;
    _tableB.dataSource = self;
    [self.view addSubview:_tableB];
    
    _tableC = [[UITableView alloc]initWithFrame:CGRectMake(Width * (- 100), Height * -40, 0, 0) style:UITableViewStylePlain];
    _tableC.layer.cornerRadius = 10;
    _tableC.layer.masksToBounds = YES;
    _tableC.delegate = self;
    _tableC.dataSource = self;
    [_scroll1 addSubview:_tableC];
    
    
    
    AFHTTPRequestOperationManager *manager1 = [AFHTTPRequestOperationManager manager];
    [manager1 GET:@"http://watch-cdn.idailywatch.com/api/list/brands/zh-hans?page=1&ver=iphone&app_ver=9" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *arr1 = (NSArray *)responseObject;
        
        for (NSDictionary *dic1 in arr1) {
            BrandsModel *mine2 = [[BrandsModel alloc]init];
            mine2.image1 = [dic1 objectForKey:@"cover_landscape_hd"];
            mine2.title = [dic1 objectForKey:@"title"];
            mine2.pubdate1 = [dic1 objectForKey:@"pubdate"];
            mine2.summary = [dic1 objectForKey:@"summary"];
            mine2.content = [dic1 objectForKey:@"content"];
            [self.array5 addObject:mine2];
            //[_array5New addObject:mine2];
            self.array5New = [NSMutableArray arrayWithArray:self.array5];
            [_array5New retain];
            NSLog(@"%@",_array5);
        
        
        
            
            
        }
        
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                NSLog(@"error = %@", error);
        
    }];
    
}

#pragma mark -
#pragma mark 下雪事件
-(void)ontime{
    UIImageView *view = [[UIImageView alloc] initWithImage:_pic];//声明一个UIImageView对象，用来添加图片
    view.alpha = 0.5;//设置该view的alpha为0.5，半透明的
    int x = round(random()%320);//随机得到该图片的x坐标
    int y = round(random()%320);//这个是该图片移动的最后坐标x轴的
    int s = round(random()%15)+10;//这个是定义雪花图片的大小
    int sp = 1/round(random()%100)+1;//这个是速度
    view.frame = CGRectMake(x, -50, s, s);//雪花开始的大小和位置
    [self.view addSubview:view];//添加该view
    [UIView beginAnimations:nil context:view];//开始动画
    [UIView setAnimationDuration:10*sp];//设定速度
    view.frame = CGRectMake(y, 500, s, s);//设定该雪花最后的消失坐标
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];
}

#pragma mark -
#pragma mark 回首页

-(void)buttonSelect:(id)sender
{
[UIView animateWithDuration:1 animations:^{
    _tableC.frame = CGRectMake(10, 100, 100, 200);
    _tableC.backgroundColor = [UIColor whiteColor];
    _tableC.alpha = 0.3;
    
    _buttonSelect.frame = CGRectMake(10, 70, 100, 30);
} completion:nil];
    

}
#pragma mark -
#pragma mark 回首页
- (void)buttonReturnHome
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark -
#pragma mark searchBar开始编辑事件
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
    _tableB.frame = CGRectMake(0, Height * 40, Width * 320, Height * 200);
}


#pragma mark -
#pragma mark 获取商标中文名
-(void)getArray2
{

    for (int i = 0; i < _array5.count; i ++) {
        NSString *str1 = [_array5[i]title];
        NSArray *arra5 = [str1 componentsSeparatedByString:@" "];
        [_arraya6 addObject:arra5.lastObject];
        
        
    }

}
#pragma mark -
#pragma mark searchBar点击筛选事件
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.array5New = [NSMutableArray arrayWithArray:_array5];
    _tableB.frame = CGRectMake(Width * -80, Height * -80, 0, 0);

//   NSLog(@"%@",_searchBar.text);
    Brandsbr2ViewController *vc = [[Brandsbr2ViewController alloc]init];
    vc.getNum = _searchBar.text;
    vc.array6 = self.array5;
    [self.navigationController pushViewController:vc animated:YES];
    //[_arraya7 removeAllObjects];
    

}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [_searchBar endEditing:YES];
}
#pragma mark -
#pragma mark searchBar弹出
-(void)searchAction:(UINavigationItem *)item
{
[_searchBar resignFirstResponder];
    if (_buttonItem1.accessibilityViewIsModal == YES) {
        
        _searchBar.placeholder = @"请输入要搜索的品牌名";
        _buttonItem1.accessibilityViewIsModal = NO;
        //_buttonItem1.title = @"取消";
        UIButton *button8 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button8 setTitle:@"取消" forState:UIControlStateNormal];
        _buttonItem1 = [[UIBarButtonItem alloc]initWithCustomView:button8];
        
    } else if(_buttonItem1.accessibilityViewIsModal == NO)
    {
    
        //_searchBar.frame = CGRectMake(0, 0, 0, 0);
        _searchBar.placeholder = @"请输入要搜索的品牌名";
        _buttonItem1.accessibilityViewIsModal = YES;
    
       
    }
        
     _tableB.frame = CGRectMake(0, 0, 0, 0);
   

}
#pragma mark -
#pragma mark Actionsheet响应无品牌事件
-(void)buttonActionNo:(UIButton *)button
{

    UIActionSheet *action = [[UIActionSheet alloc]initWithTitle:@"温馨提示" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"确定" otherButtonTitles:@"请选择其他品牌", nil];
    action.delegate = self;
    [action showInView:self.scroll1];
    

}

#pragma mark -
#pragma mark 记录scrollview滚动时的即时偏移量
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{

_aView.frame = CGRectMake(Width * (0 + (160 - scrollView.contentOffset.x)), Height * 35, Width * 600, Height * 345) ;

}
#pragma mark -
#pragma mark button响应事件
-(void)buttonAction:(UIButton *)button
{
    

    Brandsbr1ViewController *vc = [[Brandsbr1ViewController alloc]init];
    _buttonContent = [NSString stringWithFormat:@"%@",button.currentTitle
                     ];
    vc.getcontent = [_buttonContent substringToIndex:1];
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
}
#pragma mark -
#pragma mark cell数量
-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView == _tableB) {
        if (tableView == _tableB) {
            
            
            return _arraya7.count;
            //NSLog(@"%ld",_arraya7.count);
            //[_arraya7 removeAllObjects];
            
        }
        return 0;
    }
    else if(tableView == _tableC)
    {
    
        return 9;
    
    }
    return 0;
}
#pragma mark -
#pragma mark 编辑searchBar弹出tableView
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [_arraya8 removeAllObjects];
//    NSLog(@"666666666666666666%@",_arraya7);
    [_tableB reloadData];
    
    
    if (_searchBar.text.length > 0) {
        
        //NSLog(@"🀄️🀄️🀄️🀄️🀄️🀄️🀄️🀄️🀄️🀄️%@",_array5New);
        _tableB.frame = CGRectMake(0, Height * 40, Width * 320, Height * 200);
        [_tableB reloadData];
        
        [self.arrayamiddle removeAllObjects];
        
    for (int i = 0; i < _array5New.count; i ++) {
       // NSLog(@"%@", [_array5New[i] title]);
        
        

        if([[_array5New[i] title] rangeOfString:_searchBar.text].location != NSNotFound || [[_array5New[i] title] rangeOfString:[_searchBar.text lowercaseString]].location != NSNotFound)
        
        {
            
            //[self.arrayamiddle addObject:[_array5[i]title]];
        
            [self.arrayamiddle addObject:[_array5New[i] title]];
            
            //self.arraya7 = [NSMutableArray arrayWithObjects:[_array5New[i] title], nil];
            
            // NSLog(@"%@", _arraya7);
            [_tableB reloadData];
            [self reloadInputViews];
        } else{
        
        
            //[_array5New removeObjectAtIndex:i];
        
        }
        
        
        
    }
        
        self.arraya7 = [NSMutableArray arrayWithArray:self.arrayamiddle];
        NSLog(@"数组的元素是%@", self.arraya7);
        [_tableB reloadData];
    }
    if(_searchBar.text.length == 0)
    {
    //_searchBar.text = @"";
    _tableB.frame = CGRectMake(Width * -80, Height * -80, 0, 0);
    
   
    }
    NSLog(@"🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌%@",_array5New);
}
#pragma mark -
#pragma mark cell样式
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tableB) {
        if (tableView == _tableB) {
            
            
            _cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
            _cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            _cell.textLabel.text = _arraya7 [indexPath.row];
            _cell.textLabel.font = [UIFont systemFontOfSize:12];
            
            return _cell;
            
        }
        return nil;
    }
    else if(tableView == _tableC )
    {
        UITableViewCell *cell2 = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
        
        cell2.textLabel.text = _backgroundArray[indexPath.row];
        cell2.textLabel.font = [UIFont systemFontOfSize:12];
        return cell2;
    
    
    }
    return nil;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == _tableB) {
        return 30;
    }
    
    return 30;
}
#pragma mark -
#pragma mark SearchBar内容和tableView内容一致
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tableB) {
        [_searchBar resignFirstResponder];
        
        
        NSArray *arrl = [_arraya7[indexPath.row] componentsSeparatedByString:@" "];
        [_arraya8 addObject:arrl.lastObject];
        //[arrl retain];
        //NSLog(@"%ld",indexPath.row);
        _searchBar.text = [_arraya8 lastObject];
        Brandsbr2ViewController *vc = [[Brandsbr2ViewController alloc]init];
        vc.getNum = [_arraya8 lastObject];
        vc.array6 = self.array5;
        [self.navigationController pushViewController:vc animated:YES];
        
        [_tableB reloadData];
    }
    else if(tableView == _tableC)
    {
        _labled1.alpha = 0.5;
        if (indexPath.row <= 2) {
            if (indexPath.row == 0) {
                [_scroll1 setBackgroundColor:[UIColor blackColor]];
                [_labled1 setBackgroundColor:[UIColor blackColor]];
                
            }else if (indexPath.row == 1) {
                [_scroll1 setBackgroundColor:[UIColor colorWithRed:254/255.0 green:111/255.0 blue:94/255.0 alpha:1]];
                [_labled1 setBackgroundColor:[UIColor colorWithRed:254/255.0 green:111/255.0 blue:94/255.0 alpha:1]];
            }else if (indexPath.row == 2) {
                [_scroll1 setBackgroundColor:[UIColor cyanColor]];
                [_labled1 setBackgroundColor:[UIColor cyanColor]];
            }
           
        }
        if (indexPath.row >=3) {
            [_labled1 setBackgroundColor:[UIColor blackColor]];
    
        [_scroll1 setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:[NSString stringWithFormat:@"背景%d.jpeg",indexPath.row]]]];
        }
        [UIView animateWithDuration:1 animations:^{
            _tableC.frame = CGRectMake(10, 80, 0, 0);
            _buttonSelect.frame = CGRectMake(Width * 10, Height *70, Width *80, Height *30);
        } completion:nil];
        
    
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
