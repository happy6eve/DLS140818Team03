//
//  Brandsbr2ViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-15.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "Brandsbr2ViewController.h"
#import "NewsFifthTableViewCell.h"
#import "NewsForthTableViewCell.h"
#import "UIButton+WebCache.h"
#import "NewsModel.h"
#import "NewsSixthTableViewCell.h"
#import <ShareSDK/ShareSDK.h>

@interface Brandsbr2ViewController ()<UIWebViewDelegate>

@end

@implementation Brandsbr2ViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _array4 = [[NSMutableArray alloc]init];
        _array6 = [[NSMutableArray alloc]init];
        _arrNew = [[NSMutableArray alloc]init];
        _arrNew1 = [[NSMutableArray alloc]init];
        _arr1 = [[NSArray alloc]init];
    }
    return self;
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *meta = [NSString stringWithFormat:@"document.getElementsByName(\"viewport\")[0].content = \"width=%f, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no\"", webView.frame.size.width];
    [webView stringByEvaluatingJavaScriptFromString:meta];
    [webView stringByEvaluatingJavaScriptFromString:
     @"var tagHead =document.documentElement.firstChild;"
     "var tagMeta = document.createElement(\"meta\");"
     "tagMeta.setAttribute(\"http-equiv\", \"Content-Type\");"
     "tagMeta.setAttribute(\"content\", \"text/html; charset=utf-8\");"
     "var tagHeadAdd = tagHead.appendChild(tagMeta);"];
    [webView stringByEvaluatingJavaScriptFromString:
     @"var tagHead =document.documentElement.firstChild;"
     "var tagStyle = document.createElement(\"style\");"
     "tagStyle.setAttribute(\"type\", \"text/css\");"
     "tagStyle.appendChild(document.createTextNode(\"BODY{padding: 20pt 15pt}\"));"
     "var tagHeadAdd = tagHead.appendChild(tagStyle);"];
    [webView stringByEvaluatingJavaScriptFromString:
     @"var script = document.createElement('script');"
     "script.type = 'text/javascript';"
     "script.text = \"function ResizeImages() { "
     "var myimg,oldwidth;"
     "var maxwidth=320;"
     "for(i=0;i <document.images.length;i++){"
     "myimg = document.images;"
     "if(myimg.width > maxwidth){"
     "oldwidth = myimg.width;"
     "myimg.width = maxwidth;"
     "myimg.height = myimg.height * (maxwidth/oldwidth);"
     "}"
     "}"
     "}\";"
     "document.getElementsByTagName('head')[0].appendChild(script);"];
    [webView stringByEvaluatingJavaScriptFromString:@"ResizeImages();"];

    webView.frame = CGRectMake(0, Height * 175 , Width * 320,  webView.scrollView.contentSize.height + 200);
    [_table5 reloadData];
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
     self.navigationController.navigationBar.translucent = NO;
//    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(Width * 80.0f, 0.0f,  Width * 160.0f, Height * 44.0f)];
//    imageView.image = [UIImage imageNamed:@"LOGO1.png"];
//    [self.navigationController.navigationBar addSubview:imageView];

    _alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"没有你选择的品牌" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
     [self.view addSubview:_alert];
    
    
    
    
    
    _table5 = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * 568) style:UITableViewStylePlain];
    _table5.delegate = self;
    _table5.dataSource = self;
    [self.view addSubview:_table5];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, Width * 60, Height * 30);
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回1.png"] forState:UIControlStateNormal];
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回2.png"] forState:UIControlStateHighlighted];
    [button setTitle:@"返回" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button setTintColor:[UIColor whiteColor]];
    [button addTarget:self action:@selector(buttonActionBrand1) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = item;

//    NSLog(@"%@",_number);
    if (_number) {
        
    
    _str = [_arr1[[_number intValue] - 100]pubdate1] ;
    _str1 = [_arr1[[_number intValue] - 100]title];
    _str2 = [_arr1[[_number intValue] - 100]summary];
    _str3 = [_arr1[[_number intValue] - 100]content];
    _str4 = [_arr1[[_number intValue] - 100]image1];
//        NSLog(@"%@",[_arr1[[_number intValue] - 100] title]);
    }
    if(_getNum){
       for (int i = 0; i < _array6.count; i ++) {
      
        
        NSString * str = [_array6[i] title];
        NSArray * arr = [str componentsSeparatedByString:@" "];
        
        [_arrNew1 addObject:arr.lastObject];
        //NSLog(@"%@", _arrNew1[i]);
          
        
    }
    for (int j = 0; j < _arrNew1.count; j ++) {
        if ([_getNum isEqualToString:_arrNew1[j]]) {
            _str = [_array6[j]pubdate1] ;
            _str1 = [_array6[j]title];
            _str2 = [_array6[j]summary];
            _str3 = [_array6[j]content];
            _str4 = [_array6[j]image1];
            
            _num = j;
            
            break;
    }
            }
    if (![_getNum isEqualToString:_arrNew1[_num]]) {
        [_alert show];
    }
    
    }
    

}
- (void)buttonActionBrand1
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    buttonIndex = 3;
    [self.navigationController popViewControllerAnimated:YES];

}
-(IBAction)textFieldDoneEditing:(UIAlertView *)sender
{
    [sender resignFirstResponder];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return 2;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        NewsSixthTableViewCell *cell = [[NewsSixthTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
        cell.backgroundColor = [UIColor lightGrayColor];
//        [cell.dbutton1 setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[_arr1[[_number intValue]- 100] image1]]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"2.png"]];
        [cell.dbutton1 setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_str4]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"2.png"]];
        cell.dbutton1.layer.transform = CATransform3DMakeRotation(1, 0, 0,0 );
        cell.selectionStyle = NO;
        [cell.dlable1 setText:[NSString stringWithFormat:@"品牌:%@",_str1]];
        [cell.dlable2 setText:[NSString stringWithFormat:@"发行时间:%@",_str]];
        [cell.dlable3 setText:[NSString stringWithFormat:@"%@",_str2]];
        [cell.dlable3 setFont:[UIFont systemFontOfSize:12]];
        [cell.dlable1 setFont:[UIFont systemFontOfSize:13]];
        [cell.dlable2 setFont:[UIFont systemFontOfSize:9]];
        cell.backgroundColor = [UIColor colorWithRed:48 / 255.0 green:48 / 255.0 blue:48 / 255.0 alpha:1];

                           return cell;
        }
    else if(indexPath.row == 1){
    
    static NSString * identifier = @"cell2";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
            
            
            _webView1 = [[UIWebView alloc]initWithFrame:CGRectMake(0, Height * 0, Width * 320, Height * 600)];
            _webView1.delegate = self;
            _webView1.scalesPageToFit = YES;
            _webView1.scrollView.scrollEnabled = NO;
            [self.table5 addSubview:_webView1];//<font size=30><font/>
            NSString *str1 = @"<font size=12>";
            NSString *str2 = _str3;
            NSString *str4 = @"<font/>";
            NSString  *str = [NSString stringWithFormat:@"%@%@%@",str1,str2,str4];
            
            _webView1.backgroundColor = [UIColor whiteColor];
            
            [_webView1 loadHTMLString:str baseURL:nil];
        }
        
     
        UILabel *elable1 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 2, Height * 200, Width * 316, Height * 50)];
        elable1.backgroundColor = [UIColor blackColor];
        elable1.alpha = 0.6;
        [cell addSubview:elable1];
       
    
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
         button.frame = CGRectMake(0, Height * 465, Width * 320, Height * 40);
        button.backgroundColor = [UIColor blackColor];
        button.alpha = 0.5;
        
        [button setTitle:@"分享" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonActionBrand2) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:button];
        

        
        return cell;
    
    }
    return 0;

}
- (void)buttonActionBrand2
{
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"share"  ofType:@"jpg"];
    
    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:[NSString stringWithFormat:@"%@\n看到一个好宝贝,分享给大家😊", _str2]
                                       defaultContent:@"默认分享内容，没内容时显示"
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:@"ShareSDK"
                                                  url:@"http://www.sharesdk.cn"
                                          description:@"这是一条测试信息"
                                            mediaType:SSPublishContentMediaTypeNews];
    
    [ShareSDK showShareActionSheet:nil
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions: nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(@"分享成功");
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_SHARE_FAI", @"发布失败!error code == %d, error code == %@"), [error errorCode], [error errorDescription]);
                                }
                            }];

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (indexPath.row == 0) {
        return Height * 170;
    }
    return  _webView1.scrollView.contentSize.height + 100;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    [tableView deselectRowAtIndexPath:indexPath animated:NO];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
