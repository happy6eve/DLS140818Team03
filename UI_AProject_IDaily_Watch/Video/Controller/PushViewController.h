//
//  PushViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-13.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoModel.h"

@interface PushViewController : UIViewController
@property (nonatomic ,retain)UITableView * tableView;
@property (nonatomic ,retain)UIWebView * webView;
@property (nonatomic ,assign)NSInteger index;
@property (nonatomic ,retain)NSMutableArray * dataSource;
@property (nonatomic ,retain)VideoModel *mode;
@end
