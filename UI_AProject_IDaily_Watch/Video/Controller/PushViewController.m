//
//  PushViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-13.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import "PushViewController.h"
#import "PUSHCELLViewController.h"
#import "UIButton+WebCache.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MediaToolbox/MediaToolbox.h>

@interface PushViewController ()<UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate>
@property(nonatomic,retain)MPMoviePlayerController *moveplayer;
@end

@implementation PushViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.dataSource = [NSMutableArray array];
    }
    return self;
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
//    NSString *meta = [NSString stringWithFormat:@"document.getElementsByName(\"viewport\")[0].content = \"width=%f, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no\"", webView.frame.size.width];
//    [webView stringByEvaluatingJavaScriptFromString:meta];
//    [webView stringByEvaluatingJavaScriptFromString:
//     @"var tagHead =document.documentElement.firstChild;"
//     "var tagMeta = document.createElement(\"meta\");"
//     "tagMeta.setAttribute(\"http-equiv\", \"Content-Type\");"
//     "tagMeta.setAttribute(\"content\", \"text/html; charset=utf-8\");"
//     "var tagHeadAdd = tagHead.appendChild(tagMeta);"];
//    [webView stringByEvaluatingJavaScriptFromString:
//     @"var tagHead =document.documentElement.firstChild;"
//     "var tagStyle = document.createElement(\"style\");"
//     "tagStyle.setAttribute(\"type\", \"text/css\");"
//     "tagStyle.appendChild(document.createTextNode(\"BODY{padding: 20pt 15pt}\"));"
//     "var tagHeadAdd = tagHead.appendChild(tagStyle);"];
//    [webView stringByEvaluatingJavaScriptFromString:
//     @"var script = document.createElement('script');"
//     "script.type = 'text/javascript';"
//     "script.text = \"function ResizeImages() { "
//     "var myimg,oldwidth;"
//     "var maxwidth=320;"
//     "for(i=0;i <document.images.length;i++){"
//     "myimg = document.images;"
//     "if(myimg.width > maxwidth){"
//     "oldwidth = myimg.width;"
//     "myimg.width = maxwidth;"
//     "myimg.height = myimg.height * (maxwidth/oldwidth);"
//     "}"
//     "}"
//     "}\";"
//     "document.getElementsByTagName('head')[0].appendChild(script);"];
//    [webView stringByEvaluatingJavaScriptFromString:@"ResizeImages();"];

    webView.frame = CGRectMake(0, Height * 260, Width * 320, webView.scrollView.contentSize.height);
    [self.tableView reloadData];
    
    
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem * item = [[UIBarButtonItem alloc]initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(buttonAction:)];
    
    self.navigationItem.leftBarButtonItem = item;
    [self CreateTableView];
    // Do any additional setup after loading the view.
}
#pragma mark-
#pragma mark 创建Tableview
-(void)CreateTableView
{
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * (568 - 64)) style:UITableViewStyleGrouped];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_tableView];
    [_tableView release];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, Width * 60, Height * 30);
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回1.png"] forState:UIControlStateNormal];
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回2.png"] forState:UIControlStateHighlighted];
    [button setTitle:@"返回" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button setTintColor:[UIColor whiteColor]];
    [button addTarget:self action:@selector(buttonActionVideo) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = item;
    
    
    
}
- (void)buttonActionVideo
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark-
#pragma mark TableView datasource deleget
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        
        static NSString * indentifer = @"cell";
        PUSHCELLViewController  * cell = [tableView dequeueReusableCellWithIdentifier:indentifer];
        if (!cell) {
            cell = [[[PUSHCELLViewController alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:indentifer]autorelease];
        }
        NSLog(@"%lu", (long)_index);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (_index == 0) {
            
            [cell.button setBackgroundImageWithURL:[NSURL URLWithString:[_dataSource[_index]bigimage2]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"1.png"]];
        }
        else
        {
            [cell.button setBackgroundImageWithURL:[NSURL URLWithString:[_dataSource[_index]videoimage]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"1.png"]];
            
        }
        
        [cell.videoButton addTarget:self action:@selector(vidaoAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.toplabel.text = [_dataSource[_index]source];
        cell.titelabel.text = [_dataSource[_index]title];
        cell.daylabel.text = [_dataSource[_index]days];
        cell.subtitelabel.text = [_dataSource[_index]maker];
   
//        NSURL * url = [NSURL URLWithString:[_dataSource[_index]videoUrl]];
//        
//        [cell.web loadRequest:[NSURLRequest requestWithURL:url]];
         //[cell.web reload];

        return cell;
    }
    else
    {
        static NSString * indentifer = @"cell11";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:indentifer];
        if (!cell) {
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:indentifer]autorelease];
            _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, Height * 260, Width * 320, Height * 200)];
            _webView.scrollView.scrollEnabled = NO;
            self.webView.delegate =self;
            
            NSString * html = [_dataSource[_index]content];
            [_webView loadHTMLString:html baseURL:nil];
            [self.tableView addSubview:_webView];
        }
    
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
      
        return cell;
        
    }
    return 0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return Height * 260;
    }
    return _webView.scrollView.contentSize.height;
    
}
-(void)vidaoAction:(id)sender
{

    NSString *url = [_dataSource[_index]videoUrl] ;
    NSURL *videoUrl=[NSURL URLWithString:url];
    _moveplayer=[[MPMoviePlayerController alloc]initWithContentURL:videoUrl];
    [_moveplayer prepareToPlay];
    _moveplayer.controlStyle = MPMovieControlStyleFullscreen;
    _moveplayer.view.frame = CGRectMake(0, 0, Width * 320, Height * 180);
    [self.tableView addSubview:_moveplayer.view];
    
    
    
   
    
}
-(void)buttonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated
{
    [_moveplayer stop];
    [_moveplayer release];
    NSLog(@"视频关闭,内存释放成功!");
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
