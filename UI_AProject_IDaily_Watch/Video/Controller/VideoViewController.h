//
//  VideoViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-13.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPRequestOperationManager.h"

@interface VideoViewController : UIViewController
@property (nonatomic ,retain)UITableView * tableView1;
@property (nonatomic ,assign)int count;
@property (nonatomic ,retain)NSMutableArray * dataSource;
@property (nonatomic , assign)NSInteger button1tag;
@property (nonatomic , assign)NSInteger button2tag;
@end
