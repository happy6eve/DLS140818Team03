//
//  VideoViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-13.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import "VideoViewController.h"
#import "TOPTableViewCell.h"
#import "ContentTableViewCell.h"
#import "PushViewController.h"
#import "VideoModel.h"
#import "VideogetData.h"
#import "UIButton+WebCache.h"
#import "MJRefresh.h"

@interface VideoViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation VideoViewController
- (void)dealloc
{
    [_tableView1 release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        VideogetData * data = [[VideogetData alloc]init];
        _count = 2;
        [data getDataFromBlock:^(NSMutableArray* dataSource) {
            self.dataSource = dataSource;
            [_tableView1 reloadData];
           
                   }];
        // Custom initialization
    }
    return self;
}
///添加监听事件 当页面载入时显示tablebar
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    
    self.tabBarController.tabBar.hidden = NO;
//    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    [self CreateTableView];
    [self setupRefresh];
    
    //左上角button
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.frame = CGRectMake(0, 0, Width * 30, Height * 30);
    [button1 setBackgroundImage:[UIImage imageNamed:@"TodayHome2.png"] forState:UIControlStateNormal];
    [button1 setBackgroundImage:[UIImage imageNamed:@"TodayHome1.png"] forState:UIControlStateHighlighted];
    [button1 addTarget:self action:@selector(buttonReturnHome) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc]initWithCustomView:button1];
    self.navigationItem.leftBarButtonItem = item1;
    // Do any additional setup after loading the view.
}
#pragma mark-
#pragma mark 创建Tableview
-(void)CreateTableView
{

    self.tableView1 = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * (568 - 64)) style:UITableViewStyleGrouped];
    self.tableView1.dataSource = self;
    self.tableView1.delegate = self;
    self.tableView1.backgroundColor = [UIColor colorWithRed:48 /255.0 green:48 / 255.0 blue:48 / 255.0 alpha:1];
    self.tableView1.separatorStyle = UITableViewCellSelectionStyleNone;
    self.tableView1.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_tableView1];
    self.navigationController.navigationBar.translucent = NO;
//    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(Width * 80.0f, 0.0f, Width * 160.0f, Height * 44.0f)];
//    imageView.image = [UIImage imageNamed:@"LOGO1.png"];
//    [self.navigationController.navigationBar addSubview:imageView];
    [_tableView1 release];
    
    
    
    
    
}
#pragma mark - 
#pragma mark 回首页
- (void)buttonReturnHome
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark-
#pragma mark TableView datasource deleget
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return ((_dataSource.count - 1) /2 + 1) ;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        
        static NSString * indentifer = @"cell";
       TOPTableViewCell  * cell = [tableView dequeueReusableCellWithIdentifier:indentifer];
        if (!cell) {
            cell = [[[TOPTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:indentifer]autorelease];
        }
                
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
         [cell.button addTarget:self action:@selector(ButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        NSURL * url = [NSURL URLWithString:[_dataSource[0]videoimage]];
        [cell.button setBackgroundImageWithURL:url forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"1.png"]];
       
        cell.label.text = [_dataSource[0]title];
    
        return cell;
    }
   else
   {
       static NSString * indentifer = @"cell11";
       ContentTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:indentifer];
       if (!cell) {
           cell = [[[ContentTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:indentifer]autorelease];
       }
             cell.selectionStyle = UITableViewCellSelectionStyleNone;
       cell.contentView.backgroundColor = [UIColor colorWithRed:48 /255.0 green:48 / 255.0 blue:48 / 255.0 alpha:1];
       [cell.button1 addTarget:self action:@selector(ButtonAction1:) forControlEvents:UIControlEventTouchUpInside];
       [cell.button2 addTarget:self action:@selector(ButtonAction2:) forControlEvents:UIControlEventTouchUpInside];
       NSURL * url1 = [NSURL URLWithString:[_dataSource[(indexPath.row - 1)* 2 +1 ]bigimage]];
       [cell.button1 setBackgroundImageWithURL:url1 forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"1.png"]];
       cell.label1.text = [_dataSource[(indexPath.row - 1)* 2 +1 ]title];
        cell.timeLabel1.text = [_dataSource[(indexPath.row - 1)* 2 +1 ]days];
       NSURL * url2 = [NSURL URLWithString:[_dataSource[(indexPath.row - 1)* 2 +2]bigimage ]];
       cell.button1.tag = (indexPath.row - 1) * 2 +1;
       cell.button2.tag = (indexPath.row) * 2 ;
       
       [cell.button2 setBackgroundImageWithURL:url2 forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"1.png"]];
        cell.label2.text = [_dataSource[(indexPath.row - 1)* 2 +2 ]title];
        cell.timeLabel2.text = [_dataSource[(indexPath.row - 1)* 2 +2 ]days];
   
       return cell;

   }
    return 0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
  
    return 0.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 160 *Height;
    }
    return 210  * Height;
    
}
#pragma mark-
#pragma mark button点击事件
-(void)ButtonAction:(id)sender
{
       
    PushViewController * vc = [[PushViewController alloc]init];
    vc.dataSource = _dataSource;
    vc.index = 0;
       [self.navigationController pushViewController:vc  animated:NO];
    self.tabBarController.tabBar.hidden = YES;
}
-(void)ButtonAction1:(UIButton*)sender
{
    
    PushViewController * vc = [[PushViewController alloc]init];
    [self.navigationController pushViewController:vc  animated:YES];
    vc.dataSource = _dataSource;
    vc.index = sender.tag;
  
    self.tabBarController.tabBar.hidden = YES;
}

-(void)ButtonAction2:(UIButton *)sender
{
    
    PushViewController * vc = [[PushViewController alloc]init];
    
    [self.navigationController pushViewController:vc  animated:YES];
    vc.dataSource = _dataSource;
   vc.index = sender.tag;
    self.tabBarController.tabBar.hidden = YES;
}

#pragma mark -
#pragma mark 刷新
- (void)setupRefresh
{
    // 1.下拉刷新(进入刷新状态就会调用self的headerRereshing)
    [self.tableView1 addHeaderWithTarget:self action:@selector(headerRereshing)];
   // [self.tableView1 headerBeginRefreshing];
    
    // 2.上拉加载更多(进入刷新状态就会调用self的footerRereshing)
    [self.tableView1 addFooterWithTarget:self action:@selector(footerRereshing)];
    //[self.tableView1 footerEndRefreshing];
    
    // 设置文字(也可以不设置,默认的文字在MJRefreshConst中修改)
    self.tableView1.headerPullToRefreshText = @"下拉以上弦...";
    self.tableView1.headerReleaseToRefreshText = @"松开即可上弦...";
    self.tableView1.headerRefreshingText = @"上弦中...";

    self.tableView1.footerPullToRefreshText = @"上拉可以加载更多数据了";
    self.tableView1.footerReleaseToRefreshText = @"松开马上加载更多数据了";
    self.tableView1.footerRefreshingText = @"加载中";
}

#pragma mark 开始进入刷新状态
- (void)headerRereshing
{
    
    // 2.2秒后刷新表格UI
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 刷新表格
        //[self.tableView1 reloadData];
        
     
        [_tableView1 reloadData];
        
        // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
        [self.tableView1 headerEndRefreshing];
    });
}
- (void)footerRereshing
{
 
   // NSMutableArray *  datamodel = [NSMutableArray array];
    AFHTTPRequestOperationManager * manager1 = [AFHTTPRequestOperationManager manager];
    [manager1 GET:[NSString stringWithFormat:@"http://watch-cdn.idailywatch.com/api/list/video/zh-hans?page=%d&ver=iphone&app_ver=9" , _count] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        for (NSDictionary * dic in responseObject) {
            VideoModel * model = [[VideoModel alloc]init];
            model.title = [dic objectForKey:@"title"];
           model.bigimage  = [dic objectForKey:@"cover_thumb_hd"];
            model.videoimage = [dic objectForKey:@"cover_hd"];
            model.days = [dic objectForKey:@"pubdate"];
            model.maker = [dic objectForKey:@"author"];
            model.source = [dic objectForKey:@"source"];
            NSString * video = [dic objectForKey:@"link_video"];
            NSRange range = [video rangeOfString:@".mp4"];
            model.videoUrl = [video substringToIndex:range.location + range.length];
            model.content = [dic objectForKey:@"content"];
            [_dataSource addObject:model];
            
            
        }
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    

    
    // 2.2秒后刷新表格UI
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 刷新表格
        [self.tableView1 reloadData];
        
        // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
        [self.tableView1 footerEndRefreshing];
        _count += 1;
    });
   
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
