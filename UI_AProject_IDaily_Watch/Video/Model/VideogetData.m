//
//  getData.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-14.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "VideogetData.h"
#import "VideoModel.h"

#import "AFHTTPRequestOperationManager.h"
@implementation VideogetData
- (void)getDataFromBlock:(MyBlock)block
{
    
    
    self.dataBlock = block;
    self.datamodel = [NSMutableArray array];
    AFHTTPRequestOperationManager * manager1 = [AFHTTPRequestOperationManager manager];
  
    [manager1 GET:@"http://watch-cdn.idailywatch.com/api/list/video/zh-hans?page=1&ver=iphone&app_ver=9" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        for (NSDictionary * dic in responseObject) {
            VideoModel * model = [[VideoModel alloc]init];
            model.title = [dic objectForKey:@"title"];
            model.bigimage = [dic objectForKey:@"cover_thumb_hd"];
              model.videoimage = [dic objectForKey:@"cover_hd"];
            model.bigimage2 = [dic objectForKey:@"cover_portrait_hd"];
            model.days = [dic objectForKey:@"pubdate"];
            model.maker = [dic objectForKey:@"author"];
            model.source = [dic objectForKey:@"source"];
            NSString * video = [dic objectForKey:@"link_video"];//先获取
            NSRange range = [video rangeOfString:@".mp4"];//找map4range
                     model.videoUrl = [video substringToIndex:range.location + range.length];//读取子字符串
            model.content = [dic objectForKey:@"content"];
            [_datamodel addObject:model];
           
            
        }
         self.dataBlock(_datamodel);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    
    
    
        //self.dataBlock(_datamodel);
  
}
@end
