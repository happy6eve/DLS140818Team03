//
//  Model.h
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-14.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoModel : NSObject
@property (nonatomic ,retain)NSString *title;
@property (nonatomic ,retain)NSString *bigimage;
@property (nonatomic ,retain)NSString *bigimage2;
@property (nonatomic ,retain)NSString *videoimage;
@property (nonatomic ,retain)NSString *days;
@property (nonatomic ,retain)NSString *maker;
@property (nonatomic ,retain)NSString *videoUrl;
@property (nonatomic ,retain)NSString *content;
@property (nonatomic ,retain)NSString *source;
@end
