//
//  PUSHCELLViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-14.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PUSHCELLViewController : UITableViewCell
@property(nonatomic ,retain)UIButton * button;
@property(nonatomic ,retain)UILabel *toplabel;
@property(nonatomic ,retain)UILabel *titelabel;
@property(nonatomic ,retain)UILabel *subtitelabel;
@property(nonatomic ,retain)UILabel *daylabel;
@property(nonatomic ,retain)UIButton *videoButton;
@end
