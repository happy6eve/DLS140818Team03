//
//  TOPTableViewCell.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-13.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import "TOPTableViewCell.h"
#import "PushViewController.h"

@implementation TOPTableViewCell
- (void)dealloc
{
    [_button release];
    [_label release];
    [super dealloc];
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button.frame = CGRectMake(0, 0, Width * 320, Height * 160);
        [self.button setBackgroundImage:[UIImage imageNamed:@"1111.png"] forState:UIControlStateNormal];
       
        [self.contentView addSubview:_button];
        
        self.label = [[UILabel alloc]initWithFrame:CGRectMake(0, Height * 130, Width * 320, Height * 30)];
        self.label.backgroundColor = [UIColor colorWithWhite:0.2f alpha:0.6f]; // label透明
        self.label.textColor = [UIColor whiteColor];
        self.label.text = @"Shape Your Time : 卡拉亚制表时光隧道";
        [self.contentView addSubview:_label];
        
        
        
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
