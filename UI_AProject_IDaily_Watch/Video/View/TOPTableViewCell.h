//
//  TOPTableViewCell.h
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-13.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TOPTableViewCell : UITableViewCell
@property (nonatomic ,retain)UIButton * button;
@property (nonatomic ,retain)UILabel * label;
@end
