//
//  ContentTableViewCell.h
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-13.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentTableViewCell : UITableViewCell
@property(nonatomic ,retain)UIButton * button1;
@property(nonatomic ,retain)UIButton * button2;
@property(nonatomic ,retain)UILabel * label1;
@property(nonatomic ,retain)UILabel * timeLabel1;
@property(nonatomic ,retain)UILabel * label2;
@property(nonatomic ,retain)UILabel * timeLabel2;
@end
