//
//  PUSHCELLViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-14.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import "PUSHCELLViewController.h"

@interface PUSHCELLViewController ()

@end

@implementation PUSHCELLViewController

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Custom initialization
       // self.web = [[UIWebView alloc]init];
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button.frame = CGRectMake(0, 0, Width * 320, Height * 180);
      [self.button setBackgroundImage:[UIImage imageNamed:@"1111.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_button];
        
        
        self.toplabel = [[UILabel alloc]initWithFrame:CGRectMake(Width * 10, Height * 190,  Width *200, Height * 15)];
        self.toplabel.text = @"Girard-Perregaux";
        
        self.toplabel.font = [UIFont systemFontOfSize:12];
        self.toplabel.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:_toplabel];
        
        self.titelabel = [[UILabel alloc]initWithFrame:CGRectMake(Width * 10, Height * 205, Width * 320, Height * 25)];
        self.titelabel.text = @"三桥拖飞轮: 柏芝全新钛金属桥表";
        [self.contentView addSubview:_titelabel];
        
        self.subtitelabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 230, 200, 20)];
        self.subtitelabel.font = [UIFont systemFontOfSize:12];
        self.subtitelabel.textColor = [UIColor lightGrayColor];
        self.subtitelabel.text = @"撰稿 X Fijji 编辑 X 兰斯洛特";
        [self.contentView addSubview:_subtitelabel];
        
        self.daylabel = [[UILabel alloc]initWithFrame:CGRectMake(Width * 190, Height * 230, Width * 120, Height * 20)];
        self.daylabel.font = [UIFont systemFontOfSize:12];
        self.daylabel.textColor = [UIColor lightGrayColor];
        self.daylabel.text = @"August 18.2014";
        [self.contentView addSubview:_daylabel];

        self.videoButton = [[UIButton alloc]initWithFrame:CGRectMake(Width * 120, Height * 50, Width * 80, Height * 80)];
        [self.videoButton setBackgroundImage:[UIImage imageNamed:@"TodayVedio.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_videoButton];
        
    }
    return self;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
