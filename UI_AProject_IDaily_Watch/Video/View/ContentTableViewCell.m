//
//  ContentTableViewCell.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-13.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import "ContentTableViewCell.h"



@implementation ContentTableViewCell

- (void)dealloc
{
    [_button1 release];
    [_button2 release];
    [_label1 release];
    [_timeLabel1 release];
    [super dealloc];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button1.frame = CGRectMake(Width * 10, Height * 5, Width * 145, Height * 110);
        self.button1.layer.shadowColor = [UIColor blackColor].CGColor;
        self.button1.layer.shadowOpacity = 1;
        self.button1.layer.cornerRadius = 10;
        self.button1.layer.masksToBounds = YES;
      
        [self.contentView addSubview:_button1];
        self.button2 = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button2.frame = CGRectMake(Width * 165, Height * 45, Width * 145, Height * 110);
        self.button2.layer.cornerRadius = 10;
        self.button2.layer.masksToBounds = YES;
        [self.button2 setBackgroundImage:[UIImage imageNamed:@"2222.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_button2];
        
        self.label1 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 10, Height * 120, Width * 145, Height * 50)];
        self.label1.backgroundColor = [UIColor clearColor];
        self.label1.textColor = [UIColor whiteColor];
        self.label1.numberOfLines = 0;
        self.label1.font = [UIFont systemFontOfSize:12];
        self.label1.text = @"Shape Your Time : 卡拉亚制表时光隧道";
        [self.contentView addSubview:_label1];
        
        self.timeLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 10, Height * 150, Width * 145, Height * 30)];
       // self.timeLabel.backgroundColor = [UIColor whiteColor];
        self.timeLabel1.textColor = [UIColor whiteColor];
        self.timeLabel1.numberOfLines = 0;
        self.timeLabel1.font = [UIFont systemFontOfSize:10];
        self.timeLabel1.text = @"October 13,2014";
        [self.contentView addSubview:_timeLabel1];
        
        self.label2 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 165, Height * 160, Width * 145, Height * 50)];
        self.label2.backgroundColor = [UIColor clearColor];
        self.label2.textColor = [UIColor whiteColor];
        self.label2.numberOfLines = 0; //自动换行
        self.label2.font = [UIFont systemFontOfSize:12];
        self.label2.text = @"Shape Your Time : 卡拉亚制表时光隧道";
        [self.contentView addSubview:_label2];
        
        self.timeLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 165, Height * 190, Width * 145, Height * 30)];
        // self.timeLabel.backgroundColor = [UIColor whiteColor];
        self.timeLabel2.textColor = [UIColor whiteColor];
        self.timeLabel2.numberOfLines = 0;
        self.timeLabel2.font = [UIFont systemFontOfSize:10];
        self.timeLabel2.text = @"October 13,2014";
        [self.contentView addSubview:_timeLabel2];
        
        
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
