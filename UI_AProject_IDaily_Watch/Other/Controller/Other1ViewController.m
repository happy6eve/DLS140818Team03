//
//  Other1ViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-21.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "Other1ViewController.h"
#import "OtherFirstTableViewCell.h"
#import "UIButton+WebCache.h"
#import "Other2ViewController.h"
#import "NewsModel.h"
#import "DataBaseManager.h"

@interface Other1ViewController ()

@end

@implementation Other1ViewController




-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{

    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
        
//         NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
//        NSLog(@"%@",path);
//        
//        
//        
//        //获取文件路径
//        NSString *filePath = [NSString stringWithFormat:@"%@/daily(title).txt",path];
//
//        //读取文件中得数组
//        
//        self.arrx = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithFile:filePath]];
//        self.arrx1 = [NSMutableArray arrayWithArray:_arrx];
//        NSLog(@"☁️☁️☁️☁️☁️☁️☁️☁️☁️☁️☁️%@",_arrx);
//        for (NewsModel *lalal in _arrx) {
//            NSLog(@"%@",lalal.title);
//        }
        
        //[_arrx removeAllObjects];
        self.arrx = [NSMutableArray arrayWithArray:[DataBaseManager select]];
        NSLog(@"%@",_arrx);
            }
    return self;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _tableOther = [[UITableView alloc]initWithFrame:CGRectMake(0, 0,Width * 320,Height * (568 - 64)) style:UITableViewStylePlain];
    _tableOther.delegate = self;
    _tableOther.dataSource = self;
    [self.view addSubview:_tableOther];
    
   
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = CGRectMake(0, 0, Width * 60, Height * 30);
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回1"] forState:UIControlStateNormal];
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回2"] forState:UIControlStateHighlighted];
    [button setTitle:@"返回" forState:UIControlStateNormal];
    [button setTintColor:[UIColor whiteColor]];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = item;
    [button addTarget:self action:@selector(buttonReturnOther) forControlEvents:UIControlEventTouchUpInside];
    [item release];

}
- (void)buttonReturnOther
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark -
#pragma mark cell个数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_arrx.count == 0) {
        return 0;
    }
    return _arrx.count;

}
#pragma mark -
#pragma mark cell样式
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   
    
    OtherFirstTableViewCell *cell = [[OtherFirstTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    
    cell.lableOther1.text =  [_arrx[indexPath.row] title];//_arrx[indexPath.row];
    cell.lableOther1.font = [UIFont systemFontOfSize:12];
    cell.lableOther2.text = [_arrx[indexPath.row] pubdate1 ];//_arrx1[indexPath.row];
    cell.lableOther2.font = [UIFont systemFontOfSize:12];
    
    cell.lableOther3.text = [_arrx[indexPath.row] summary]; //_arrx2[indexPath.row];
    cell.lableOther3.font = [UIFont systemFontOfSize:10];
    
    NSString *str = [_arrx[indexPath.row] image1];//_arrx3[indexPath.row];
    [cell.buttonOther setBackgroundImageWithURL:[NSURL URLWithString:str] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"6.jpeg"]];
    
    cell.buttonOther.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.buttonOther.layer.shadowOpacity = 1;
    cell.selectionStyle = NO;
    return cell;

}


-(void)buttonBigAction:(UIButton *)button
{

    [self dismissViewControllerAnimated:YES completion:nil];


}

#pragma mark -
#pragma mark cell行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 110;

}
#pragma mark -
#pragma mark 启动编辑动画
-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    
    [super setEditing:editing animated:animated];
    [_tableOther setEditing:!_tableOther.editing animated:animated];
    
}
#pragma mark -
#pragma mark 允许编辑
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return YES;
}
//tableView delegate
#pragma mark -
#pragma mark 编辑方式
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    if ([[_arr objectAtIndex:indexPath.row]isEqualToString:@"空虚公子"]) {
//        return UITableViewCellEditingStyleInsert;
//    }
    return UITableViewCellEditingStyleDelete;
    
}

#pragma mark -
#pragma mark 确认编辑
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.arrx =[NSMutableArray arrayWithArray:[DataBaseManager select]];
    [tableView reloadData];
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //修改数据源
        
        //[_arrx removeObjectAtIndex:indexPath.row];
        [DataBaseManager remove:_arrx[indexPath.row]];
        
        self.arrx =[NSMutableArray arrayWithArray:[DataBaseManager select]];
        //修改UI
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
        NSLog(@"--------------%@",_arrx);
        
        [tableView reloadData];

    }
    
//    if (editingStyle == UITableViewCellEditingStyleInsert) {
//        //修改数据源
//        [_arrx insertObject:@"" atIndex:indexPath.row + 1];
//        //修改UI
//        [tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indexPath.row + 1 inSection:indexPath.section ]] withRowAnimation:UITableViewRowAnimationLeft];
//        
//    }
    
    //[tableView reloadData];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView reloadData];
    Other2ViewController *vc = [[Other2ViewController alloc]init];
    _arrx =[NSMutableArray arrayWithArray:[DataBaseManager select]];
    NSLog(@"%@",_arrx);
    [tableView reloadData];
    vc.countQ = (NSInteger )indexPath.row;
    vc.arraX1 = self.arrx;
    [self.navigationController pushViewController:vc animated:YES];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
