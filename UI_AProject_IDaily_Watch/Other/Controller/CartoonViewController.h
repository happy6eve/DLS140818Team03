//
//  CartoonViewController.h
//  UI_AProject_IDaily_Watch
//
//  Created by 刘旭 on 14/10/27.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface CartoonViewController : UIViewController
{
    BOOL _isRunning;
    NSTimer * _timer;
}
@property (nonatomic ,retain)UIImage *pic;
@end
