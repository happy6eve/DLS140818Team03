//
//  CartoonViewController.m
//  UI_AProject_IDaily_Watch
//
//  Created by 刘旭 on 14/10/27.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import "CartoonViewController.h"
#import "AppDelegate.h"

@interface CartoonViewController ()<UITableViewDataSource, UITableViewDelegate>
@property(nonatomic, retain)UITableView *tableView;

@end
@implementation CartoonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.pic = [UIImage imageNamed:@"snow.png"];
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0,Width * 320,Height * (568 - 64))];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    
    
     //左上角按钮
   UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = CGRectMake(0, 0, Width * 60, Height * 30);
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回1"] forState:UIControlStateNormal];
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回2"] forState:UIControlStateHighlighted];
    [button setTitle:@"返回" forState:UIControlStateNormal];
    [button setTintColor:[UIColor whiteColor]];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = item;
    [button addTarget:self action:@selector(buttonActionOther) forControlEvents:UIControlEventTouchUpInside];
    [item release];
    
}
- (void)buttonActionOther
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    if (indexPath.row == 0) {
        cell.textLabel.text = @"纯黑极致";
    }
    if (indexPath.row == 1) {
        cell.textLabel.text = @"动漫天空";
    }
    if (indexPath.row == 2){
        cell.textLabel.text = @"乡村味道";
    }
    if (indexPath.row == 3) {
        cell.textLabel.text = @"城市星空";
    }
    if (indexPath.row == 4) {
        cell.textLabel.text = @"银河炫彩";
    }
    if (indexPath.row == 5){
        cell.textLabel.text = @"优胜美地";
    }
    
    cell.selectionStyle = NO;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    }
    if (indexPath.row == 1) {
        self.navigationController.navigationBar.barTintColor = [UIColor  colorWithPatternImage:[UIImage imageNamed:@"nav1.jpg"]];
    }
    if (indexPath.row == 2){
       self.navigationController.navigationBar.barTintColor = [UIColor  colorWithPatternImage:[UIImage imageNamed:@"nav2.jpg"]];
    }
    if (indexPath.row == 3) {
        self.navigationController.navigationBar.barTintColor = [UIColor  colorWithPatternImage:[UIImage imageNamed:@"nav3.jpg"]];
    }
    if (indexPath.row == 4) {
        self.navigationController.navigationBar.barTintColor = [UIColor  colorWithPatternImage:[UIImage imageNamed:@"nav4.jpeg"]];
    }
    if (indexPath.row == 5){
       self.navigationController.navigationBar.barTintColor = [UIColor  colorWithPatternImage:[UIImage imageNamed:@"nav5.jpg"]];
    }
   
}
- (void)grayBackground
{
    self.navigationController.navigationBar.barTintColor = [UIColor grayColor];
}
- (void)purpleBackground
{
    self.navigationController.navigationBar.barTintColor = [UIColor purpleColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
