//
//  Other2ViewController.h
//  UI_AProject_IDaily_Watch
//
//  Created by lianglide on 14-10-24.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Other2ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,retain)NSMutableArray *arraX1;
@property(nonatomic,assign)NSInteger countQ;
@property(nonatomic,retain)UITableView *tableFinal;
@end
