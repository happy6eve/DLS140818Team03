//
//  OtherSettingViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14/10/21.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "OtherSettingViewController.h"
#import "MakerViewController.h"
#import "Other1ViewController.h"

@interface OtherSettingViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation OtherSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
//    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(Width * 80.0f, 0.0f, Width * 160.0f, Height * 44.0f)];
//    imageView.image = [UIImage imageNamed:@"LOGO1.png"];
//    [self.navigationController.navigationBar addSubview:imageView];
    
    
    //左上角button
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.frame = CGRectMake(0, 0, Width * 30, Height * 30);
    [button1 setBackgroundImage:[UIImage imageNamed:@"TodayHome2.png"] forState:UIControlStateNormal];
    [button1 setBackgroundImage:[UIImage imageNamed:@"TodayHome1.png"] forState:UIControlStateHighlighted];
    [button1 addTarget:self action:@selector(buttonReturnHome) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc]initWithCustomView:button1];
    self.navigationItem.leftBarButtonItem = item1;
    [item1 release];
    
    


    self.view.backgroundColor = [UIColor whiteColor];
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * (568-64)) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource =self;
    [self.view addSubview:_tableView];
    
    
    
    
    
   
    
    
    // Do any additional setup after loading the view.
}

#pragma mark -
#pragma mark 回首页
- (void)buttonReturnHome
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)settingAction:(id)sender
{
    OtherSettingViewController * vc = [[OtherSettingViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(void)kikeAction
{
    Other1ViewController *vc = [[Other1ViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    if (section == 1) {
        return 3;
    }
    if (section == 2) {
        return 2;
    }
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tableView) {
        if (indexPath.section == 0) {
            static NSString * indefinter = @"setting";
            OtherSettingTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:indefinter];
            if (!cell) {
                cell = [[OtherSettingTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:indefinter];
            }
            cell.selectionStyle = NO;
            [cell.userImage setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.image1]]]];
            cell.userNameLable.text = self.name1;
            return cell;
        }
        
            static NSString * indefinter = @"setting";
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:indefinter];
            if (!cell) {
                cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:indefinter];
            }
            cell.selectionStyle = NO;
            if (indexPath.section == 1) {
                if (indexPath.row == 0) {
                    cell.textLabel.text = @"主题切换";
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

                }
                if (indexPath.row == 1) {

                    cell.textLabel.text = @"我的收藏";
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
 
                }
                if (indexPath.row == 2) {

                    cell.textLabel.text = @"清除图片缓存";
                    
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
                    NSString *path = [paths lastObject];
                    self.cache = [self folderSizeAtPath:path];
                    
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%.1fM", self.cache];

                }
            }
            if(indexPath.section == 2)
            {

                if (indexPath.row == 0) {
                    cell.textLabel.text = @"版本";
                    cell.detailTextLabel.text = @"V1.0";
                }
                if (indexPath.row == 1) {
                    cell.textLabel.text = @"制作人员";
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
 
                }

            }
        return cell;
    }
    return 0;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return @"个人设置";
    }
    if(section == 2)
    {
        return @"关于";
    }
    return 0;
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tableView) {
        if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                
                
                CartoonViewController * vc = [[CartoonViewController alloc]init];
                [self.navigationController pushViewController:vc animated:YES];
            }
            if (indexPath.row == 1) {
                if ([DataBaseManager select].count == 0) {
                    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"提示" message:@"您还没有收藏内容" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    [alert1 show];
                }else{
                    Other1ViewController *vc = [[Other1ViewController alloc]init];
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }
            if (indexPath.row == 2) {
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"是否清除缓存" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"清除", nil];
                [alert show];
            }
        }
        if (indexPath.section == 2) {
            if (indexPath.row == 0) {
                NSURL* url = [NSURL URLWithString: @"nes:"];
                
                [[UIApplication sharedApplication] openURL: url];
            }
            if (indexPath.row == 1) {
                MakerViewController * vc = [[MakerViewController alloc]init];
                [self.navigationController pushViewController:vc  animated:YES];
                
            }
        }
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self action:nil];
        self.cache = 0;
        [self.tableView reloadData];

    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == _tableView) {
        if (section == 0) {
            return 0.000001;
        }
        
    }
    
    return 0;
    
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tableView) {
        if (indexPath.section == 0) {
            return 150;
        }
    }
    return 40;
}
#pragma mark -
#pragma mark 清空缓存
- (long long) fileSizeAtPath:(NSString*) filePath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize];
    }
    return 0;
}
//遍历文件夹获得文件夹大小，返回多少M
- (float ) folderSizeAtPath:(NSString*) folderPath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:folderPath]) return 0;
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    NSString* fileName;
    long long folderSize = 0;
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        folderSize += [self fileSizeAtPath:fileAbsolutePath];
    }
    NSLog(@"%.2f", folderSize/(1024.0*1024.0));
    return folderSize/(1024.0*1024.0);
}

- (void)action:(id)sender
{
    //彻底清除缓存第一种方法
    UIButton * button = sender;
    [button setTitle:@"清理完毕" forState:UIControlStateNormal];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *path = [paths lastObject];
    
    NSString *str = [NSString stringWithFormat:@"缓存已清除%.1fM", [self folderSizeAtPath:path]];
    NSLog(@"%@",str);
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"缓存清除成功" message:str delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
    [alert show];
 
    
    NSArray *files = [[NSFileManager defaultManager] subpathsAtPath:path];
    for (NSString *p in files) {
        NSError *error;
        NSString *Path = [path stringByAppendingPathComponent:p];
        if ([[NSFileManager defaultManager] fileExistsAtPath:Path]) {
            [[NSFileManager defaultManager] removeItemAtPath:Path error:&error];
        }
    }
    
}


-(void)clearCacheSuccess
{
    NSLog(@"清理成功");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
