//
//  OtherSettingViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14/10/21.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OtherSettingTableViewCell.h"
#import <ShareSDK/ShareSDK.h>
#import "CartoonViewController.h"
#import "DataBaseManager.h"
@interface OtherSettingViewController : UIViewController<UIAlertViewDelegate>

@property(nonatomic, retain)UITableView * tableView;
@property(nonatomic, assign) float cache;
@property(nonatomic, retain)NSString *name;
@property(nonatomic, retain)NSString *image;
@property(nonatomic, retain)NSString *name1;
@property(nonatomic, retain)NSString *image1;
@property(nonatomic, retain)UISlider *slider;
@end
