//
//  MakerViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14/10/21.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "MakerViewController.h"


@interface MakerViewController ()

@end

@implementation MakerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //设置背景图片
    UIImageView * view = [[UIImageView alloc]initWithFrame:self.view.frame];
    view.image = [UIImage imageNamed:@"maker.jpg"];
    [self.view addSubview:view];
    
    self.view.backgroundColor = [UIColor blackColor];
    //下雪图片
    self.pic = [UIImage imageNamed:@"snow.png"];
    //计时器
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(timeAction) userInfo:nil repeats:YES];
    
    //左上方按钮
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, Width * 60, Height * 30);
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回1.png"] forState:UIControlStateNormal];
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回2.png"] forState:UIControlStateHighlighted];//高亮效果
    [button setTitle:@"返回" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button setTintColor:[UIColor whiteColor]];//设置button字体颜色
    [button addTarget:self action:@selector(buttonActionOther) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = item;
    [item release];
    
    
    
    UILabel * label1 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 10,Height * 20,Width * 300,Height * 150)];
    label1.backgroundColor = [UIColor whiteColor];
    label1.alpha = 0.5;
    [self.view addSubview:label1];
    UIImageView *imageView1 = [[UIImageView alloc]initWithFrame:CGRectMake(Width * 10,Height * 40,Width *110,Height * 110)];
    [imageView1 setImage:[UIImage imageNamed:@"0.jpg"]];
    [self.view addSubview:imageView1];
    UILabel *label11 = [[UILabel alloc]initWithFrame:CGRectMake(Width* 140,Height * 40,Width * 150,Height * 30)];
    label11.text = @"刘旭";
    label11.font = [UIFont systemFontOfSize:14.0];
    [self.view addSubview:label11];
    UILabel *label111 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 140,Height *  (40 + 30) ,Width * 170,Height * 30)];
    label111.text = @"QQ：472443138";
    label111.font = [UIFont systemFontOfSize:14.0];
    [self.view addSubview:label111];
    UILabel *label1111 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 140,Height * (40 + 30 + 30),Width * 150,Height * 50)];
    label1111.text = @"留言： 跟我走";
    label1111.font = [UIFont systemFontOfSize:14.0];
    [self.view addSubview:label1111];
    
    UILabel * label2 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 10,Height * (20 + 150 + 20),Width * 300,Height * 150)];
    label2.backgroundColor = [UIColor whiteColor];
    label2.alpha = 0.5;
    [self.view addSubview:label2];
    UIImageView *imageView2 = [[UIImageView alloc]initWithFrame:CGRectMake(Width * 10,Height * (40 + 150 + 20),Width * 110,Height * 110)];
    [imageView2 setImage:[UIImage imageNamed:@"1.jpg"]];
    [self.view addSubview:imageView2];
    UILabel *label22 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 140,Height * (40 + 150 + 20), 150, 30)];
    label22.text = @"徐继垚";
    label22.font = [UIFont systemFontOfSize:14.0];
    [self.view addSubview:label22];
    UILabel *label222 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 140,Height * (40 + 150 + 20 + 30), Width * 150,Height * 30)];
    label222.text = @"QQ：422242216";
    label222.font = [UIFont systemFontOfSize:14.0];
    [self.view addSubview:label222];
    UILabel *label2222 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 140,Height * (40 + 150 + 20 + 30 + 30),Width * 150, Height * 60)];
    label2222.text = @"留言：                        天真岁月不忍欺，青春荒唐我不负你";
    label2222.numberOfLines = 0;
    label2222.font = [UIFont systemFontOfSize:14.0];
    [self.view addSubview:label2222];
    
    UILabel * label3 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 10,Height *( 20 + 150 + 20 + 150 + 20), Width * 300,Height * 150)];
    label3.backgroundColor = [UIColor whiteColor];
    label3.alpha = 0.5;
    [self.view addSubview:label3];
    UIImageView *imageView3 = [[UIImageView alloc]initWithFrame:CGRectMake(Width * 10,Height * (40 + 150 + 20 + 150 + 20),Width * 110,Height * 110)];
    [imageView3 setImage:[UIImage imageNamed:@"2.jpg"]];
    [self.view addSubview:imageView3];
    UILabel *label33 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 140,Height *( 40 + 150 + 20 + 150 + 20), Width * 150,Height * 30)];
    label33.text = @"梁立德";
    label33.font = [UIFont systemFontOfSize:14.0];
    [self.view addSubview:label33];
    UILabel *label333 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 140,Height * (40 + 150 + 20 + 150 + 20 + 30), Width * 150,Height * 30)];
    label333.text = @"QQ:1759453558";
    label333.font = [UIFont systemFontOfSize:14.0];
    [self.view addSubview:label333];
    UILabel *label3333 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 140,Height * (40 + 150 + 20 + 150 + 20 + 30 +30), Width * 150, Height * 60)];
    label3333.text = @"留言：                        海纳百川，有容乃大；壁立千仞，无欲则刚";
    label3333.numberOfLines = 0;
    label3333.font = [UIFont systemFontOfSize:14.0];
    [self.view addSubview:label3333];
    
}
-(void)timeAction{
    UIImageView *view = [[UIImageView alloc] initWithImage:_pic];//声明一个UIImageView对象，用来添加图片
    view.alpha = 0.4;//设置该view的alpha为0.5，半透明的
    int x = round(random()%320);//随机得到该图片的x坐标
    int y = round(random()%320);//这个是该图片移动的最后坐标x轴的
    int s = round(random()%15)+10;//这个是定义雪花图片的大小
    int sp = 1/round(random()%100)+1;//这个是速度
    view.frame = CGRectMake(x, -50, s, s);//雪花开始的大小和位置
    [self.view addSubview:view];//添加该view
    [UIView beginAnimations:nil context:view];//开始动画
    [UIView setAnimationDuration:10*sp];//设定速度
    view.frame = CGRectMake(y, 500, s, s);//设定该雪花最后的消失坐标
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];
}
- (void)buttonActionOther
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
