//
//  Other2ViewController.m
//  UI_AProject_IDaily_Watch
//
//  Created by lianglide on 14-10-24.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import "Other2ViewController.h"
#import "NewsModel.h"
#import "NewsForthTableViewCell.h"
#import "UIButton+WebCache.h"


@interface Other2ViewController ()<UIWebViewDelegate>
@property(nonatomic ,retain)UIWebView *webVView;
@end

@implementation Other2ViewController


-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{

    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.arraX1 = [[NSMutableArray alloc]init];
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _tableFinal = [[UITableView alloc]initWithFrame:CGRectMake(0, 0,Width * 320, Height * (568 - 64) ) style:UITableViewStylePlain];
    _tableFinal.delegate = self;
    _tableFinal.dataSource = self;
    [self.view addSubview:_tableFinal];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = CGRectMake(0, 0, Width * 60, Height * 30);
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回1"] forState:UIControlStateNormal];
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回2"] forState:UIControlStateHighlighted];
    [button setTitle:@"返回" forState:UIControlStateNormal];
    [button setTintColor:[UIColor whiteColor]];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = item;
    [button addTarget:self action:@selector(buttonReturnOther1) forControlEvents:UIControlEventTouchUpInside];
    [item release];
}

- (void)buttonReturnOther1
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_arraX1.count == 0) {
        return 0;
    }
    return 2;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if (indexPath.row == 0) {
        
    
    NewsForthTableViewCell *cell = [[NewsForthTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    [cell.bbutton11 setBackgroundImageWithURL:[NSURL URLWithString:[_arraX1[_countQ] image2]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"6.jpeg"]];
    cell.blable1.text = [_arraX1[_countQ] title];
    cell.blable2.text = [_arraX1[_countQ] pubdate1];
    cell.blable4.text = [_arraX1[_countQ] summary];
    
    
    return cell;
    }
    static NSString * identifier = @"celltwo";
    if (indexPath.row == 1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
            _webVView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * 100)];
            _webVView.scalesPageToFit = YES;
            _webVView.scrollView.scrollEnabled = NO;
            _webVView.delegate = self;
            [cell.contentView addSubview:_webVView];
            NSString *str1 = [_arraX1[_countQ]content];
            NSString *str2 = @"<font size=30>";
            NSString *str3 = @"<font/>";
            NSString  *str = [NSString stringWithFormat:@"%@%@%@",str2,str1,str3];
            _webVView.backgroundColor = [UIColor orangeColor];
            //NSLog(@"%d",temp++);
            [_webVView loadHTMLString:str baseURL:nil];        }
        return cell;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return Height * 260;
        
    }
    return _webVView.scrollView.contentSize.height ;

}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    [webView stringByEvaluatingJavaScriptFromString:
     @"var script = document.createElement('script');"
     "script.type = 'text/javascript';"
     "script.text = \"function ResizeImages() { "
     "var myimg,oldwidth;"
     "var maxwidth=320;"
     "for(i=0;i <document.images.length;i++){"
     "myimg = document.images;"
     "if(myimg.width > maxwidth){"
     "oldwidth = myimg.width;"
     "myimg.width = maxwidth;"
     "myimg.height = myimg.height * (maxwidth/oldwidth);"
     "}"
     "}"
     "}\";"
     "document.getElementsByTagName('head')[0].appendChild(script);"];
    [webView stringByEvaluatingJavaScriptFromString:@"ResizeImages();"];

    webView.frame = CGRectMake(0, 0, Width * 320,   webView.scrollView.contentSize.height);
    [_tableFinal reloadData];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
