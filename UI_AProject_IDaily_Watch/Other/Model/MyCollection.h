//
//  MyCollection.h
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-21.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyCollection : NSObject
@property(nonatomic,retain)NSString *title;
@property(nonatomic,retain)NSString *time;
@property(nonatomic,retain)NSString *summary;

@end
