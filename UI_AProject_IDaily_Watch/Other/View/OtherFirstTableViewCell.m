//
//  OtherFirstTableViewCell.m
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-21.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "OtherFirstTableViewCell.h"

@implementation OtherFirstTableViewCell

- (void)awakeFromNib {
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _buttonOther = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _buttonOther.frame = CGRectMake(5, 5, 80, 80);
        
        _buttonOther.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_buttonOther];
        _lableOther1 = [[UILabel alloc]initWithFrame:CGRectMake(100, 5, 180, 30)];
        _lableOther1.backgroundColor = [UIColor clearColor];
        _lableOther1.numberOfLines = 0;
        [self.contentView addSubview: _lableOther1];
        
        _lableOther2 = [[UILabel alloc]initWithFrame:CGRectMake(100, 40, 180, 20)];
        _lableOther2.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview: _lableOther2];
        
        _lableOther3 = [[UILabel alloc]initWithFrame:CGRectMake(100, 65, 180, 35)];
        _lableOther3.numberOfLines = 0;
        _lableOther3.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview: _lableOther3];
        
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
