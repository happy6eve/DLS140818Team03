//
//  OtherFirstTableViewCell.h
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-21.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherFirstTableViewCell : UITableViewCell
@property(nonatomic,retain)UIButton *buttonOther;
@property(nonatomic,retain)UILabel *lableOther1;
@property(nonatomic,retain)UILabel *lableOther2;
@property(nonatomic,retain)UILabel *lableOther3;
@end
