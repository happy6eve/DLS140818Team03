//
//  OtherSettingTableViewCell.h
//  UI_AProject_IDaily_Watch
//
//  Created by 刘旭 on 14-10-23.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherSettingTableViewCell : UITableViewCell
@property (nonatomic ,retain)UIImageView * backgroundImage;
@property (nonatomic ,retain)UIImageView * userImage;
@property (nonatomic ,retain)UILabel * userNameLable;
@property (nonatomic ,retain)UIButton * button1;
@property (nonatomic ,retain)UIButton * button2;
@end
