//
//  TodaySecondViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-14.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "TodaySecondViewController.h"

@interface TodaySecondViewController ()<UIScrollViewDelegate>
@end
@implementation TodaySecondViewController
- (void)dealloc
{
    [_scrollView release];
    [_dataArrOne release];
    [_dataArrTwo release];
    [super dealloc];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
        self.dataArrTwo = [NSMutableArray array];
        self.dataArrOne = [NSMutableArray array];//接收总数组
        self.dataArrFour = [NSMutableArray array];//传递cover_thumb
        
    }
    return self;
}

- (void)viewDidLoad
{
    [self getData];
    [self RightBarButtonItem];
    [self LeftBarButtonItem];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.view.backgroundColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"TodaySecond.jpg"]];
    
}

- (void)getData
{
    //FOR IN遍历 第一页传来的数据
        for (NSDictionary *dic in _dataArrOne) {
            TodaySecond *sub = [[TodaySecond alloc]init];
            sub.cover_landscape = [dic objectForKey:@"cover_landscape"];
            sub.title = [dic objectForKey:@"title"];
            sub.source = [dic objectForKey:@"source"];
            [_dataArrTwo addObject:sub];
        }
    
        self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * (480 + 30))];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.contentSize = CGSizeMake(Width * 320 * 40, Height * (480 + 30));
        _scrollView.delegate = self;
    //if tag传来的为0
    if (_countOffset == 0) {
        _scrollView.contentOffset = CGPointMake(Width * _countOffset * 320, 0);
    }
    else
    _scrollView.contentOffset = CGPointMake(Width * (_countOffset - 1)* 320, 0);
    
        [self.view addSubview:_scrollView];
    //循环创建40个button
        for (int i = 0; i < 40; i++) {
            self.button = [UIButton buttonWithType:UIButtonTypeCustom];
            _button.frame = CGRectMake(Width * 320 * i, 0, Width * 320, Height * (480 - 60 - 10));
            _button.tag = i + 1000;//给40个button赋值tag
            //当tag为1000时
//            if (_button.tag == 1001) {
//                [_button addTarget:self action:@selector(buttonActionIndex1) forControlEvents:UIControlEventTouchUpInside];
//            }
//            else
            [_button addTarget:self action:@selector(buttonActionIndex2:) forControlEvents:UIControlEventTouchUpInside];
  
            [_button setBackgroundImageWithURL:[NSURL URLWithString:[_dataArrTwo[i]cover_landscape]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"1.png"]];//赋图片
            [_scrollView addSubview:_button];

            
            self.label1 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 320 * i, Height *   (480 - 60 - 10) , Width * 320, Height * 50)];
            _label1.font = [UIFont systemFontOfSize:16];
            _label1.backgroundColor = [UIColor  colorWithRed:48 / 255.0 green:48 / 255.0 blue:48 / 255.0 alpha:1];
            _label1.textAlignment = NSTextAlignmentCenter;
            _label1.text = [_dataArrTwo[i]title];
            _label1.textColor = [UIColor whiteColor];
            [self.scrollView addSubview:_label1];
            
            self.label2 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 320 * i, Height * (480 - 20 - 60 + 20 + 20 + 10), Width * 320, Height * 56)];
            _label2.font = [UIFont systemFontOfSize:16];
            _label2.backgroundColor = [UIColor  colorWithRed:48 / 255.0 green:48 / 255.0 blue:48 / 255.0 alpha:1];
            _label2.textAlignment = NSTextAlignmentCenter;
            _label2.textColor = [UIColor whiteColor];
            _label2.text = [_dataArrTwo[i]source];
            [self.scrollView addSubview:_label2];
        }
}
#pragma mark -
#pragma mark 右上角按钮
- (void)RightBarButtonItem
{
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    button2.frame = CGRectMake(Width * 270, 0, Width * 30, Height * 30);
    [button2 setBackgroundImage:[UIImage imageNamed:@"Today列表2.png"] forState:UIControlStateNormal];
    [button2 setBackgroundImage:[UIImage imageNamed:@"Today列表1.png"] forState:UIControlStateHighlighted];
    [button2 addTarget:self action:@selector(ListAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc]initWithCustomView:button2];
    self.navigationItem.rightBarButtonItem = item2;
}
#pragma mark -
#pragma mark 左上角按钮
- (void)LeftBarButtonItem
{
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.frame = CGRectMake(0, 0, Height * 30, Width * 30);

    [button1 setBackgroundImage:[UIImage imageNamed:@"TodayHome2.png"] forState:UIControlStateNormal];
    [button1 setBackgroundImage:[UIImage imageNamed:@"TodayHome1.png"] forState:UIControlStateHighlighted];
    [button1 addTarget:self action:@selector(IndexAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc]initWithCustomView:button1];
    self.navigationItem.leftBarButtonItem = item1;
    [item1 release];
}
#pragma mark -
#pragma mark 右上角按钮触发事件
- (void)ListAction
{
    TodayListViewController *vc = [[TodayListViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    vc.dataArrOne = self.dataArrOne;//1页面总数据 赋给 list页
}
#pragma mark -
#pragma mark 左上角按钮触发方法
- (void)IndexAction
{
     [self.navigationController popToRootViewControllerAnimated:YES];

}
#pragma mark -
#pragma mark 1-40页跳转
//1页面触发方法
//- (void)buttonActionIndex1
//{
//    NSString *url = [_videodata[1]videoUrl];
//    NSLog(@"%@", [_videodata[1]videoUrl]);
//    NSURL *videoUrl=[NSURL URLWithString:url];
//    MPMoviePlayerViewController *movieVc=[[MPMoviePlayerViewController alloc]initWithContentURL:videoUrl];
//    //弹出播放器
//    [self presentMoviePlayerViewControllerAnimated:movieVc];
//}
//2-40页面触发方法
- (void)buttonActionIndex2:(UIButton *)sender
{
    
    TodayListSubViewController *vc = [[TodayListSubViewController alloc]init];
    vc.hidesBottomBarWhenPushed = YES;
    self.dataArrThree = [[self.dataArrOne objectAtIndex:sender.tag - 1000]objectForKey:@"album"];
    [self.dataArrFour removeAllObjects];//防止下一页面button叠加创建, 移除对象重新填充元素
    
    //for in 遍历下一页面所需数据
    for (NSDictionary *dic in self.dataArrThree) {
        TodayListSubCellOne *sub = [[TodayListSubCellOne alloc]init];
        sub.cover_thumb1 = [dic objectForKey:@"cover_thumb"];
        sub.cover_landscape = [dic objectForKey:@"cover_landscape"];
        [self.dataArrFour addObject:sub];
    }
    vc.arr = self.dataArrFour; //将取到的cover_thumb 传递到新数组里
    vc.countList = self.dataArrFour.count;//将数组的个数传给count
    vc.count = sender.tag; //tag传给下一页
    [self.navigationController pushViewController:vc animated:YES];
    [vc release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
