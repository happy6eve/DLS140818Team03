//
//  TodaySecondViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-14.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TodayViewController.h"
#import "TodayListSubViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "UIButton+WebCache.h"
#import "TodaySecond.h"
#import "TodayListSubCellOne.h"
#import "TodayListSubCellOneTableViewCell.h"
@interface TodaySecondViewController : UIViewController 
@property(nonatomic, retain)UIScrollView *scrollView;
@property(nonatomic, retain)UILabel *label1;
@property(nonatomic, retain)UILabel *label2;
@property(nonatomic, retain)UIButton *button;
@property(nonatomic, retain)NSMutableArray *dataArrOne;///接收数组
@property(nonatomic, retain)UITableView *tableView;
@property(nonatomic, retain)NSMutableArray *videodata;//接收1页面的视频源
//@property(nonatomic, retain)NSMutableArray *videodata1;//接收2页面的视频源
@property(nonatomic, retain)NSMutableArray *dataArrTwo;
@property(nonatomic, retain)NSMutableArray *dataArrThree;//传递到listSub
@property(nonatomic, retain)NSMutableArray *dataArrFour;//存储cover_thumb
@property(nonatomic, assign)NSInteger countOffset;

@end
