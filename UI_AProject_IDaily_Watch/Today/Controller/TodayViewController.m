//
//  TodayViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-13.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "TodayViewController.h"


@interface TodayViewController ()<UITableViewDataSource, UITableViewDelegate>
@property(nonatomic ,retain)UIButton* button1;

@end

@implementation TodayViewController
- (void)dealloc
{
    [_tableView release];
    [_dataOne release];
    [_dataTwo release];
    [_dataArrZong release];
    [super dealloc];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self getData];
        self.dataArrZong = [NSMutableArray array];
        self.dataOne = [NSMutableArray array];
        self.dataTwo = [NSMutableArray array];
        

    }
    
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self tableView1];
    self.view.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    
    
    
    ///news页面navigation
    NewsNeViewController * main = [[NewsNeViewController alloc]init];
    NewsViewController* news = [[NewsViewController alloc]init];
    WikiViewController * right = [[WikiViewController alloc]init];
    //创建抽屉菜单 初始化
    _sideViewController = [[YRSideViewController alloc]initWithNibName:nil bundle:nil];
    // NewsNe2ViewController * right = [[NewsNe2ViewController alloc]init];
    //设置抽屉左右页面
    _sideViewController.rootViewController = main;
    _sideViewController.leftViewController = news;
    _sideViewController.rightViewController = right;
    _sideViewController.leftViewShowWidth = Width * 200;
    _sideViewController.needSwipeShowMenu = true;//默认开启的可滑动展示
    [self switchToChangeAnimate];
    ///news页面左上角按钮
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, Width * 30, Height * 30);
    [button setBackgroundImage:[UIImage imageNamed:@"TodayHome2.png"] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"TodayHome1.png"] forState:UIControlStateHighlighted];
    [button addTarget:self action:@selector(buttonAction1) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:button];
    _sideViewController.navigationItem.rightBarButtonItem = item;
    [item release];
    //右上角按钮
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    button2.frame = CGRectMake(Width * 270, 0, Width * 30, Height *30);
    [button2 setBackgroundImage:[UIImage imageNamed:@"Today列表2.png"] forState:UIControlStateNormal];
    [button2 setBackgroundImage:[UIImage imageNamed:@"Today列表1.png"] forState:UIControlStateHighlighted];
    [button2 addTarget:self action:@selector(ITEMAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc]initWithCustomView:button2];
    
    _sideViewController.navigationItem.leftBarButtonItem = item2;
    [item2 release];

  
    
    
    
}
#pragma mark - 
#pragma mark 去news页面
-(void)buttonAction1
{
    [_sideViewController.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark-
#pragma mark 抽屉滑动手势方法
//支持滑动手势
- (void)switchValueChange {
    
    YRSideViewController *sideViewController=[[YRSideViewController alloc]init];
    [sideViewController setNeedSwipeShowMenu:YES];
}
//改变滑动动画
- (void)switchToChangeAnimate
{
    
//    AppDelegate *delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
//    YRSideViewController *sideViewController=[delegate sideViewController];
    
    [_sideViewController setRootViewMoveBlock:^(UIView *rootView, CGRect orginFrame, CGFloat xoffset) {
        //使用简单的平移动画
        rootView.frame=CGRectMake(xoffset, orginFrame.origin.y, orginFrame.size.width, orginFrame.size.height);
    }];
}

-(void)ITEMAction:(UIButton *)sender
{
    
    
    if (![sender isSelected]) {
        [_sideViewController showLeftViewController:true];
        
        
        
        
        
            }

   //    TodayViewController *delegate=(TodayViewController*)[[UIApplication sharedApplication]delegate];
//    YRSideViewController *sideViewController=[delegate sideViewController];
    
   // [self.view];
    
    
}

#pragma mark -
#pragma mark 获取数据
- (void)getData
{
    //首页cellOne数据获取
    AFHTTPRequestOperationManager * manager1 = [AFHTTPRequestOperationManager manager];
    [manager1 GET:@"http://watch-cdn.idailywatch.com/api/list/cover/zh-hans?page=1&ver=iphone&app_ver=9" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableArray * data = [NSMutableArray arrayWithArray:responseObject];
//        [data removeObjectAtIndex:5];
        for (NSDictionary * dic in data) {
            self.dataArrZong = data;
            TodayVedio *button = [[TodayVedio alloc]init];
            button.cover_landscape_hd_568h = [dic objectForKey:@"cover_landscape_hd_568h"];
            [_dataOne addObject:button];
            [_tableView reloadData];
//            NSLog(@"%@", button.cover_landscape_hd_568h);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
    
    
    //原首页cellTwo
    AFHTTPRequestOperationManager *manager2 = [AFHTTPRequestOperationManager manager];
    [manager2 GET:@"http://watch-cdn.idailywatch.com/api/list/cover/zh-hans?page=1&ver=iphone&app_ver=9" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        for (NSDictionary *dic in responseObject) {
            TodayContent *today = [[TodayContent alloc]init];
            today.title = [dic objectForKey:@"title"];
            today.content = [dic objectForKey:@"content"];
            today.videoUrl = [dic objectForKey:@"link_video"];
            [_dataTwo addObject:today];
           [_tableView reloadData];
         
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
}
#pragma mark -
#pragma mark tableview
- (void)tableView1
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * 504)];
    _tableView.delegate = self;
    _tableView.dataSource =self;
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;//不显示CELL条
    self.tableView.showsVerticalScrollIndicator = NO;
    _tableView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_tableView];
    //添加手势滑到second页
    UISwipeGestureRecognizer * gesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(gestureAction)];
    [gesture setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.tableView addGestureRecognizer:gesture]; //添加手势到view
    [gesture release];
    
    //navigation黑色
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    //给navigation添加背景图片
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(Width * 80.0f, 0.0f, Width * 160.0f, Height * 44.0f)];
    imageView.image = [UIImage imageNamed:@"LOGO1.png"];
    [self.navigationController.navigationBar addSubview:imageView];
    //首页 左、右上角图标
    _button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    _button1.frame = CGRectMake(0, 0, Width * 30, Height * 30);
    [_button1 setBackgroundImage:[UIImage imageNamed:@"Today刷新2.png"] forState:UIControlStateNormal];
    [_button1 setBackgroundImage:[UIImage imageNamed:@"Today刷新1.png"] forState:UIControlStateHighlighted];
    [_button1 addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventTouchUpInside];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    button2.frame = CGRectMake(Width * 270, 0, Width * 30, Height * 30);
    [button2 setBackgroundImage:[UIImage imageNamed:@"Today列表2.png"] forState:UIControlStateNormal];
    [button2 setBackgroundImage:[UIImage imageNamed:@"Today列表1.png"] forState:UIControlStateHighlighted];
    [button2 addTarget:self action:@selector(button2Action) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc]initWithCustomView:_button1];
    self.navigationItem.leftBarButtonItem = item1;
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc]initWithCustomView:button2];
    [item1 release];
    self.navigationItem.rightBarButtonItem = item2;
    [item2 release];
}
#pragma mark -
#pragma mark 刷新按钮触发事件
- (void)refreshAction
{
    [_tableView reloadData];
    [self setupRefresh];
}
#pragma mark -
#pragma mark 刷新方法
- (void)setupRefresh
{
    // 1.下拉刷新(进入刷新状态就会调用self的headerRereshing)
    [self.tableView addHeaderWithTarget:self action:@selector(headerRereshing)];
    [self.tableView headerBeginRefreshing];
    
    // 2.上拉加载更多(进入刷新状态就会调用self的footerRereshing)
    //    [self.tableView addFooterWithTarget:self action:@selector(footerRereshing)];
    //    [self.tableView footerEndRefreshing];
    
    // 设置文字(也可以不设置,默认的文字在MJRefreshConst中修改)
    self.tableView.headerPullToRefreshText = @"下拉以上弦...";
    self.tableView.headerReleaseToRefreshText = @"松开即可上弦...";
    self.tableView.headerRefreshingText = @"上弦中...";
    
    self.tableView.footerPullToRefreshText = @"上拉可以加载更多数据了";
    self.tableView.footerReleaseToRefreshText = @"松开马上加载更多数据了";
    self.tableView.footerRefreshingText = @"加载中";
}
#pragma mark 开始进入刷新状态
- (void)headerRereshing
{
    // 2.2秒后刷新表格UI
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    
        // 刷新表格
        //[self.tableView1 reloadData];

        [_tableView reloadData];
        
        // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
        [self.tableView headerEndRefreshing];
    });
}

#pragma mark - 
#pragma mark cell height
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return Height * (250 + 30);
    }
    else
        return Height * (260 - 30);
}
#pragma mark - 
#pragma mark cell number
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_dataOne.count  == 0 || _dataTwo.count == 0) { //当数组为空 不创建cell。
        return 0;
    }
    return 2;
}
#pragma mark - 
#pragma mark UITableViewCell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
        static NSString *identifier = @"cellOne";
        TodayCellOneTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[TodayCellOneTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
          cell.selectionStyle = NO;
        NSURL *url = [NSURL URLWithString:[_dataOne[1]cover_landscape_hd_568h]];
        [cell.imageView1 setImageWithURL:url placeholderImage:[UIImage imageNamed:@"index.jpg"]];
        
//        [cell.vedio addTarget:self action:@selector(videoAction:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else if(indexPath.row == 1)
    {
        static NSString *identifier = @"cellTwo";
        TodayCellTwoSubTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[TodayCellTwoSubTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
        cell.selectionStyle = NO;//点击清灰
        cell.backgroundColor = [UIColor blackColor];
        [cell.button1 addTarget:self action:@selector(newsAction) forControlEvents:UIControlEventTouchUpInside];
        [cell.button2 addTarget:self action:@selector(brandsAction) forControlEvents:UIControlEventTouchUpInside];
        [cell.button3 addTarget:self action:@selector(videosAction) forControlEvents:UIControlEventTouchUpInside];
        [cell.button4 addTarget:self action:@selector(otherAction:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
        
    }
    else
        return 0;
}
#pragma mark - 
#pragma mark 首页去其它页
- (void)newsAction
{
     [self.navigationController pushViewController:_sideViewController animated:YES];
}
#pragma mark -
#pragma mark 首页去品牌页
- (void)brandsAction
{
    BrandsViewController *vc = [[BrandsViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    [vc release];
}
#pragma mark -
#pragma mark 首页去视频页
- (void)videosAction
{
    VideoViewController *vc = [[VideoViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    [vc release];
}
#pragma mark -
#pragma mark 首页去其他页
- (void)otherAction:(id)sender
{
    OtherSettingViewController *vc = [[OtherSettingViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    [vc release];
}

#pragma mark - 
#pragma mark 视频播放
//- (void)videoAction:(id)sender
//{
//    NSString *url = [_dataTwo[1]videoUrl];
//    NSURL *videoUrl=[NSURL URLWithString:url];
//    MPMoviePlayerViewController *movieVc=[[MPMoviePlayerViewController alloc]initWithContentURL:videoUrl];
//    //弹出播放器
//    [self presentMoviePlayerViewControllerAnimated:movieVc];
//}
#pragma mark -
#pragma mark 清扫手势 给Second
- (void)gestureAction
{
    TodaySecondViewController *vc = [[TodaySecondViewController alloc]init];
    vc.dataArrOne = self.dataArrZong;
    vc.videodata = self.dataTwo;
    
    [self.navigationController pushViewController:vc animated:YES];
    [vc release];
}
#pragma mark -
#pragma mark 右上角列表触发事件
- (void)button2Action
{
    TodayListViewController *vc = [[TodayListViewController alloc]init];
    vc.dataArrOne = self.dataArrZong;
    vc.dataVedio = self.dataTwo;
//    NSLog(@"%@", self.dataTwo);
    [self.navigationController pushViewController:vc animated:YES];
//    NSLog(@"%d", self.dataArrZong.count);
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
