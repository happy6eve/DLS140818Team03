//
//  TodayViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-13.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TodayCellOneTableViewCell.h"
#import "TodaySecondViewController.h"
#import "TodayListViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "TodayVedio.h"
#import "TodayContent.h"
#import "UIButton+WebCache.h"
#import "UIImageView+WebCache.h"
#import "OtherSettingViewController.h"
#import "MJRefresh.h"
#import <MediaPlayer/MediaPlayer.h>
#import <ShareSDK/ShareSDK.h>
#import "TodayCellTwoSubTableViewCell.h"
#import "AppDelegate.h"
#import "YRSideViewController.h"
#import "NewsNeViewController.h"
#import "WikiViewController.h"
#import "NewsViewController.h"
#import "BrandsViewController.h"
#import "VideoViewController.h"
@interface TodayViewController : UIViewController
@property (nonatomic, retain)UITableView *tableView;
@property (nonatomic, retain)NSMutableArray *dataOne;
@property (nonatomic, retain)NSMutableArray *dataTwo;
@property (nonatomic, retain)NSMutableArray *dataArrZong;
@property (nonatomic, retain)NSString *name;
@property (nonatomic, retain)NSString *image;
@property (nonatomic, retain)NSString *fans;
@property (nonatomic ,retain)YRSideViewController * sideViewController;
@end
