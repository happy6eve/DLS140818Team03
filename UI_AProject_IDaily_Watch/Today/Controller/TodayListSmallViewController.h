//
//  TodayListSmallViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-17.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton+WebCache.h"
#import "TodayListSubCellOne.h"
#import <ShareSDK/ShareSDK.h>
#import "UIImageView+WebCache.h"
@interface TodayListSmallViewController : UIViewController
@property(nonatomic, retain)NSMutableArray *array;//接收Album的数据数组
@property(nonatomic, assign)NSInteger count;
@end
