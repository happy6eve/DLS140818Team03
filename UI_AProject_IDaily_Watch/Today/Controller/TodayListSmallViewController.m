//
//  TodayListSmallViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-17.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "TodayListSmallViewController.h"

@interface TodayListSmallViewController ()<UIAlertViewDelegate>
@property (nonatomic ,retain)UIImageView * image;
@end

@implementation TodayListSmallViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor blackColor];
    [super viewDidLoad];
    [self imageView1];
    //左上角按钮
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = CGRectMake(0, 0, Width * 60, Height * 30);
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回1"] forState:UIControlStateNormal];
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回2"] forState:UIControlStateHighlighted];
    [button setTitle:@"返回" forState:UIControlStateNormal];
    [button setTintColor:[UIColor whiteColor]];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button addTarget:self action:@selector(buttonActionListSub) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = item;
    [item release];
    // Do any additional setup after loading the view.
}
#pragma mark - 
#pragma mark 返回TodayListSub页面
- (void)buttonActionListSub
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)imageView1
{
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = CGRectMake(0, Height * 20, Width * 320, Height * 420);
   
    [button setBackgroundImageWithURL:[NSURL URLWithString:[_array[_count - 1]cover_landscape]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"1.png"]]; //从上一页获取URL 赋图片
    button.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:button];
    
    
    //右上角保存图片按钮
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeSystem];
    button1.frame = CGRectMake(0, 0, Width * 60, Height * 30);
    button1.titleLabel.font = [UIFont systemFontOfSize:14.0];
//    [button1 setBackgroundImage:[UIImage imageNamed:@"ListShare1.png"] forState:UIControlStateNormal];
//    [button1 setBackgroundImage:[UIImage imageNamed:@"ListShare2.png"] forState:UIControlStateHighlighted];
    [button1 setTitle:@"保存图片" forState:UIControlStateNormal];
    [button1 addTarget:self action:@selector(shareAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc]initWithCustomView:button1];
    self.navigationItem.rightBarButtonItem = item1;

}
#pragma mark - 
#pragma mark 保存图片触发方法
-(void)shareAction:(UIButton * )sender
{
    _image = [[UIImageView alloc]init];
    [_image setImageWithURL:[NSURL URLWithString:[_array[_count - 1]cover_landscape]]];
   UIAlertView * aler = [[UIAlertView alloc]initWithTitle:@"提示" message:@"确定要保存么?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"保存", nil];
    [aler show];
    [self.view addSubview:aler];

    
    
    
}
#pragma mark - 
#pragma mark alterView代理方法
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        
        UIImageWriteToSavedPhotosAlbum(_image.image, nil, nil,nil);
    UIAlertView * aler2 = [[UIAlertView alloc]initWithTitle:@"提示" message:@"保存成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [aler2 show];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
