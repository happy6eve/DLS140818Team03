//
//  TodayListViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-15.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TodayListTableViewCell.h"
#import "AFHTTPRequestOperationManager.h"
#import "UIButton+WebCache.h"
#import "TodayList.h"
#import "TodayViewController.h"
#import "TodaySecondViewController.h"
#import "TodayListData.h"
#import "MJRefresh.h"
#import "TodayListSubViewController.h"
@interface TodayListViewController : UIViewController<UIAlertViewDelegate>
@property(nonatomic, retain)UITableView *tableView;
@property(nonatomic, retain)NSMutableArray *dataAarrOne;
@property(nonatomic, retain)NSMutableArray *dataArrOne;//接收数组
@property(nonatomic ,assign)NSInteger count; //将list页button.tag传给second
@property(nonatomic, retain)NSMutableArray *dataArrCount;
@property(nonatomic, assign)NSInteger countIndexPath;
@property(nonatomic, retain)NSMutableArray *dataVedio;//接收一页面的视频源
@end
