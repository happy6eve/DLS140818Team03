//
//  TodayListSubViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-16.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TodayListSubCellOneTableViewCell.h"
#import "TodayListSubCellTwoTableViewCell.h"
#import "TodayListSubCellOne.h"
#import "TodayListSubCellTwo.h"
#import "TodayListSubCellThree.h"
#import "AFHTTPRequestOperationManager.h"
#import "UIButton+WebCache.h"
#import "TodayListSmallViewController.h"
#import "TodaySecondViewController.h"
@interface TodayListSubViewController : UIViewController<UIWebViewDelegate>
@property (nonatomic, assign)NSInteger count;
@property(nonatomic, retain)NSMutableArray *dataArrOne;
@property(nonatomic, retain)NSMutableArray *dataArrTwo;
@property(nonatomic, retain)NSMutableArray *dataArrThree;
@property(nonatomic, retain)NSMutableArray *arr;
@property(nonatomic, retain)UITableView *tableView;
@property(nonatomic, retain)UIWebView *webView;
@property(nonatomic, assign)NSInteger countList;
@end
