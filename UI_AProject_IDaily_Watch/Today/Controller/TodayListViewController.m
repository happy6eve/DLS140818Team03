//
//  TodayListViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-15.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "TodayListViewController.h"

@interface TodayListViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation TodayListViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.dataArrCount = [NSMutableArray array];
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TodayListData * data = [[TodayListData alloc]init];
    [data getDataFromBlock:^(NSMutableArray* dataSource) {
        self.dataAarrOne = dataSource;
        [_tableView reloadData];
            }];
}
- (void)viewDidLoad {
    
    self.view.backgroundColor = [UIColor blackColor];
    
    [super viewDidLoad];
    [self tableView1];
    [self setupRefresh];

//    self.view.backgroundColor = [UIColor blackColor];
    // Do any additional setup after loading the view.
   
}
#pragma mark - 
#pragma mark tableView
- (void)tableView1
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * (568 - 64))];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;//不显示CELL条
    self.tableView.showsVerticalScrollIndicator = NO;
    _tableView.backgroundColor = [UIColor blackColor];
    
    //左上方按钮
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, Width * 60, Height * 30);
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回1.png"] forState:UIControlStateNormal];
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回2.png"] forState:UIControlStateHighlighted];//高亮效果
    [button setTitle:@"返回" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button setTintColor:[UIColor whiteColor]];//设置button字体颜色
    [button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = item;
    [item release];
}
#pragma mark - 
#pragma mark 返回首页
- (void)buttonAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark - 
#pragma mark cell高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_dataAarrOne.count == 0) {//防止index0崩溃
        return 0;
    }
    return _dataAarrOne.count / 5;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"cellOne";
    TodayListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[TodayListTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    cell.selectionStyle = NO;//清灰
    for (int i = 0; i < 5; i++) {
        cell.button = (MyButton *)[cell viewWithTag:i + 1];//根据tag拿Button
        cell.button.myHang = indexPath.row;//记录行数
        [cell.button setBackgroundImageWithURL:[NSURL URLWithString:[_dataAarrOne[(indexPath.row) * 5 + i]cover_thumb]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"t11.png"]];
        cell.label1 = (UILabel *)[cell viewWithTag:i + 6];
        cell.label1.text = [_dataAarrOne[(indexPath.row) * 5 + i]title];
        cell.backgroundColor = [UIColor blackColor];
        cell.label2 = (UILabel *)[cell viewWithTag:i + 11];
        cell.label2.text = [_dataAarrOne[(indexPath.row) * 5 + i]pubdate];
//        if (cell.button.tag + (cell.button.myHang) * 5 == 40) {
//            [cell.button addTarget:self action:@selector(forthAction) forControlEvents:UIControlEventTouchUpInside];
//        }
        [cell.button  addTarget:self action:@selector(buttonActionSub:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}
- (void)forthAction
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"已无图片" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
#pragma mark - 
#pragma mark 前往second页
- (void)buttonActionSub:(UIButton *)sender
{
    TodaySecondViewController *vc = [[TodaySecondViewController alloc]init];
    vc.dataArrOne = self.dataArrOne;//从首页获取数据源 属性传值 到second;
    MyButton *button =(MyButton *) sender;
    vc.countOffset = button.tag + (button.myHang) * 5;//将1-40tag传给Second页 造成对应偏移量
    vc.videodata = self.dataVedio;
    NSLog(@"%@",self.dataVedio);
//    NSLog(@"%d", vc.countOffset);
    [self.navigationController pushViewController:vc animated:YES];
    [vc release];
}

#pragma mark -
#pragma mark 刷新
- (void)setupRefresh
{
    // 1.下拉刷新(进入刷新状态就会调用self的headerRereshing)
    [self.tableView addHeaderWithTarget:self action:@selector(headerRereshing)];
    //[self.tableView headerBeginRefreshing];
    
    // 2.上拉加载更多(进入刷新状态就会调用self的footerRereshing)
//    [self.tableView addFooterWithTarget:self action:@selector(footerRereshing)];
//    [self.tableView footerEndRefreshing];
    
    // 设置文字(也可以不设置,默认的文字在MJRefreshConst中修改)
    self.tableView.headerPullToRefreshText = @"下拉以上弦...";
    self.tableView.headerReleaseToRefreshText = @"松开即可上弦...";
    self.tableView.headerRefreshingText = @"上弦中...";
    
    self.tableView.footerPullToRefreshText = @"上拉可以加载更多数据了";
    self.tableView.footerReleaseToRefreshText = @"松开马上加载更多数据了";
    self.tableView.footerRefreshingText = @"加载中";
}
#pragma mark 开始进入刷新状态
- (void)headerRereshing
{
    
    // 2.2秒后刷新表格UI
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 刷新表格
        //[self.tableView1 reloadData];
        
        
        [_tableView reloadData];
        
        // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
        [self.tableView headerEndRefreshing];
    });
}
- (void)footerRereshing
{

    // 2.2秒后刷新表格UI
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 刷新表格
        [self.tableView reloadData];
        
        // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
        [self.tableView footerEndRefreshing];
        _count += 1;
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
