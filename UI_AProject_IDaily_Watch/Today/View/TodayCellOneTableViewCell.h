//
//  TodayCellOneTableViewCell.h
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-13.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
@interface TodayCellOneTableViewCell : UITableViewCell
@property (nonatomic, retain)UIImageView *imageView1;
@property (nonatomic, retain)UIButton *vedio;
@end
