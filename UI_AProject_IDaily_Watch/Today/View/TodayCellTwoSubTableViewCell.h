//
//  TodayCellTwoSubTableViewCell.h
//  UI_AProject_IDaily_Watch
//
//  Created by 刘旭 on 14-10-23.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TodayCellTwoSubTableViewCell : UITableViewCell
@property(nonatomic, retain)UIButton *button1;
@property(nonatomic, retain)UIButton *button2;
@property(nonatomic, retain)UIButton *button3;
@property(nonatomic, retain)UIButton *button4;
@property(nonatomic, retain)UIImageView *imageView1;
@property(nonatomic, retain)UIImageView *imageView2;
@property(nonatomic, retain)UIImageView *imageView3;
@property(nonatomic, retain)UIImageView *imageView4;
@property(nonatomic, retain)UILabel *label1;
@property(nonatomic, retain)UILabel *label2;
@property(nonatomic, retain)UILabel *label3;
@property(nonatomic, retain)UILabel *label4;
@end
