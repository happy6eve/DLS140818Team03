//
//  TodayListTableViewCell.m
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-15.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "TodayListTableViewCell.h"

@implementation TodayListTableViewCell
- (void)dealloc
{
    [_background release];
    [_label1 release];
    [_label2 release];
    [_scrollView release];
    [super dealloc];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * 120)];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(Width * 110 * 5, Height * 110);
        _scrollView.showsHorizontalScrollIndicator = NO;
        [self.contentView addSubview:_scrollView];
        
        for (int i = 0; i < 5; i++) {
            self.button = [MyButton buttonWithType:UIButtonTypeSystem];
            _button.frame = CGRectMake(Width * (10 + i * 100), Height * 30, Width * 90, Height * 100);
            _button.tag = i + 1;
            _button.myHang = 1;
            [self.scrollView addSubview:_button];
            
            
            
            self.label1 = [[UILabel alloc]initWithFrame:CGRectMake(0, Height * (60 + 10), Width * 90, Height * 10)];
            _label1.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.6];
            _label1.tag = i + 6;
            _label1.textColor = [UIColor whiteColor];
            _label1.font = [UIFont systemFontOfSize:8];
            [self.button addSubview:_label1];
            
            self.label2 = [[UILabel alloc]initWithFrame:CGRectMake(0, Height * (10 + 60 + 10), Width * 90, Height * 10)];
            _label2.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.6];
            _label2.tag = i + 11;
            _label2.textColor = [UIColor whiteColor];
            _label2.font = [UIFont systemFontOfSize:8];
            [self.button addSubview:_label2];
            
        }

    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
