//
//  TodayListTableViewCell.h
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-15.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyButton.h"
@interface TodayListTableViewCell : UITableViewCell<UIScrollViewDelegate>
@property(nonatomic, retain)MyButton *button;
@property (nonatomic, retain)UILabel *background;
@property (nonatomic, retain)UILabel *label1;
@property (nonatomic, retain)UILabel *label2;
@property (nonatomic, retain)UIScrollView *scrollView;
@end
