//
//  TodayListSubCellTwoTableViewCell.h
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-16.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TodayListSubCellTwoTableViewCell : UITableViewCell
@property(nonatomic, retain)UILabel *label1;
@property(nonatomic, retain)UILabel *label2;
@property(nonatomic, retain)UILabel *label3;
@property(nonatomic, retain)UILabel *label4;
@end
