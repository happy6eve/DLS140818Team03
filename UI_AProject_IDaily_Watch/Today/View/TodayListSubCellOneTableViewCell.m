//
//  TodayListSubTableViewCell.m
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-16.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "TodayListSubCellOneTableViewCell.h"

@implementation TodayListSubCellOneTableViewCell
- (void)dealloc
{
    [_scrollView release];
    [super dealloc];
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)getCount:(NSInteger)count
{
    self.count = count;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(runTimePage) userInfo:nil repeats:YES];//定时器
    
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * 110)];
    _scrollView.pagingEnabled = YES;
    _scrollView.bounces = NO;
    [_scrollView setShowsHorizontalScrollIndicator:NO];
    _scrollView.contentSize = CGSizeMake(Width * (130 * (count + 1)), Height * 110);
    [self.contentView addSubview:_scrollView];
    for (int i = 0; i < count; i++) {
        self.button = [UIButton buttonWithType:UIButtonTypeSystem];
        _button.frame = CGRectMake(Width * (10 + 100 * i), Height * 10, Width * 90, Height * 90);
        _button.tag = i + 1;
        [self.scrollView addSubview:_button];
    }
    
    self.pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(Width * 50, Height * 50, Width * 50, Height * 30)];
    [_pageControl setNumberOfPages:count];
    _pageControl.currentPage = 0;
    _pageControl.hidden = YES;
    [_pageControl addTarget:self action:@selector(pageAction) forControlEvents:UIControlEventValueChanged];
    [self.contentView addSubview:_pageControl];
    
}
- (void)pageAction
{
    [_scrollView setContentOffset:CGPointMake(Width * (_pageControl.currentPage * 100), 0) animated:YES];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView //代理
{
    _pageControl.currentPage = scrollView.contentOffset.x / 110;
}
// pagecontrol 选择器的方法
- (void)turnPage
{
    NSInteger page = _pageControl.currentPage; // 获取当前的page
    [_scrollView scrollRectToVisible:CGRectMake(Width * 100 * (page),0, Width * 320, Height * 110) animated:NO]; // 触摸pagecontroller那个点点 往后翻一页 +1
    [_pageControl addTarget:self action:@selector(runTimePage) forControlEvents:UIControlEventValueChanged];
}
// 定时器绑定的方法
- (void)runTimePage
{
    NSInteger page = _pageControl.currentPage; // 获取当前的page
    page++;
    page = page > self.count - 1 ? 0 : page ;
    _pageControl.currentPage = page;
    [self turnPage];
}


- (void)awakeFromNib {
    // Initialization code
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
