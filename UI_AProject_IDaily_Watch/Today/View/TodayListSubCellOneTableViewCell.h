//
//  TodayListSubTableViewCell.h
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-16.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyButton.h"
@interface TodayListSubCellOneTableViewCell : UITableViewCell
@property(nonatomic, retain)UIScrollView *scrollView;
@property(nonatomic, retain)UIPageControl *pageControl;
@property(nonatomic, retain)UIButton *button;
@property(nonatomic, retain)NSTimer *timer;
- (void)getCount:(NSInteger)count;
@property(nonatomic, assign)NSInteger count;
@end
