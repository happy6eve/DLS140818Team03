//
//  TodayListSubCellTwoTableViewCell.m
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-16.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "TodayListSubCellTwoTableViewCell.h"

@implementation TodayListSubCellTwoTableViewCell
- (void)dealloc
{
    [_label1 release];
    [_label2 release];
    [_label3 release];
    [_label4 release];
    [super dealloc];
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.label1 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 10,  Height * (10 + 5), Width * 80, Height * 15)];
//        _label1.backgroundColor = [UIColor redColor];
        _label1.font = [UIFont systemFontOfSize:10];
        [self.contentView addSubview:_label1];
        _label1.textColor = [UIColor grayColor ];
        self.label2 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 10, Height * (20 + 5 + 5), Width * 270, Height * 20)];
//        _label2.backgroundColor = [UIColor blueColor];
        _label2.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:_label2];
        
        self.label3 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 10, Height * (20 + 5 + 20 + 5 + 5), Width * 160, Height * 15)];
//        _label3.backgroundColor = [UIColor orangeColor];
        _label3.font = [UIFont systemFontOfSize:10];
        [self.contentView addSubview:_label3];
        
        self.label4 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 210, Height * (20 + 5 + 20 + 5 + 5), Width * 100, Height * 15)];
//        _label4.backgroundColor = [UIColor purpleColor];
        _label4.font = [UIFont systemFontOfSize:10];
        _label4.textColor = [UIColor grayColor ];
        [self.contentView addSubview:_label4];
        
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
