//
//  TodayCellTwoSubTableViewCell.m
//  UI_AProject_IDaily_Watch
//
//  Created by 刘旭 on 14-10-23.
//  Copyright (c) 2014年 徐继垚. All rights reserved.
//

#import "TodayCellTwoSubTableViewCell.h"

@implementation TodayCellTwoSubTableViewCell
- (void)dealloc
{
    [_imageView1 release];
    [_imageView2 release];
    [_imageView3 release];
    [_imageView4 release];
    [_label1 release];
    [_label2 release];
    [_label3 release];
    [_label4 release];
    [super dealloc];
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    self.contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Back.jpg"]];
    if (self) {
        
        self.imageView1 = [[UIImageView alloc]init];
        _imageView1.frame = CGRectMake(Width * 5, Height * 5, Width * 150, Height * 105);
        _imageView1.userInteractionEnabled = YES;
        [_imageView1 setImage:[UIImage imageNamed:@"111.jpg"]];
        self.button1 = [UIButton buttonWithType:UIButtonTypeSystem];
        _button1.frame = CGRectMake(Width * 30, 0, Width * 90, Height * 90);
        [_button1 setBackgroundImage:[UIImage imageNamed:@"2副本.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_imageView1];
        [_imageView1 addSubview:_button1];
        self.label1 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 30, Height * 80, Width * 90, Height * 20)];
        _label1.text = @"腕表新闻";
        _label1.textColor = [UIColor colorWithRed:150 / 255.0 green:150 / 255.0 blue:150 / 255.0 alpha:1];
        _label1.textAlignment = NSTextAlignmentCenter;
        [_imageView1 addSubview:_label1];
        
        
        self.imageView2 = [[UIImageView alloc]init];
        _imageView2.frame = CGRectMake(Width * 160, Height * 5, Width * 150, Height * 105);
        _imageView2.userInteractionEnabled = YES;
        [_imageView2 setImage:[UIImage imageNamed:@"222.jpg"]];
        self.button2 = [UIButton buttonWithType:UIButtonTypeSystem];
        _button2.frame = CGRectMake(Width * 30, 0, Width * 90, Height * 90);
        [_button2 setBackgroundImage:[UIImage imageNamed:@"3副本.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_imageView2];
        [_imageView2 addSubview:_button2];
        self.label2 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 30, Height * 80, Width * 90, Height * 20)];
        _label2.text = @"腕表品牌";
        _label2.textColor = [UIColor colorWithRed:150 / 255.0 green:150 / 255.0 blue:150 / 255.0 alpha:1];
        _label2.textAlignment = NSTextAlignmentCenter;
        [_imageView2 addSubview:_label2];
        
        
        self.imageView3 = [[UIImageView alloc]init];
        _imageView3.frame = CGRectMake(Width * 5, Height * 115, Width * 150, Height * 105);
        [_imageView3 setImage:[UIImage imageNamed:@"333.jpg"]];
        _imageView3.userInteractionEnabled = YES;
        self.button3 = [UIButton buttonWithType:UIButtonTypeSystem];
        _button3.frame = CGRectMake(Width * 30, 0, Width * 90, Height * 90);
        [_button3 setBackgroundImage:[UIImage imageNamed:@"4副本.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_imageView3];
        [_imageView3 addSubview:_button3];
        self.label3 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 30, Height * 80, Width * 90, Height * 20)];
        _label3.text = @"腕表视频";
        _label3.textColor = [UIColor colorWithRed:150 / 255.0 green:150 / 255.0 blue:150 / 255.0 alpha:1];
        _label3.textAlignment = NSTextAlignmentCenter;
        [_imageView3 addSubview:_label3];
        
        self.imageView4 = [[UIImageView alloc]init];
        _imageView4.frame = CGRectMake(Width * 160, Height * 115, Width * 150, Height * 105);
        _imageView4.userInteractionEnabled = YES;
        _imageView4.backgroundColor = [UIColor blackColor];
        [_imageView4 setImage:[UIImage imageNamed:@"444.jpg"]];
        self.button4 = [UIButton buttonWithType:UIButtonTypeSystem];
        _button4.frame = CGRectMake(Width * 30, 0, Width * 90, Height * 90);
        [_button4 setBackgroundImage:[UIImage imageNamed:@"5副本.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_imageView4];
        [_imageView4 addSubview:_button4];
        self.label4 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 30, Height * 80, Width * 90, Height * 20)];
        _label4.text = @"我的腕表";
        _label4.textColor = [UIColor colorWithRed:150 / 255.0 green:150 / 255.0 blue:150 / 255.0 alpha:1];
        _label4.textAlignment = NSTextAlignmentCenter;
        [_imageView4 addSubview:_label4];
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
