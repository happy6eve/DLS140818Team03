//
//  MyButton.h
//  UI_代码库
//
//  Created by 安东 on 14-9-22.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyButton : UIButton
@property (nonatomic,assign)NSInteger myHang;
+ (id)buttonWithFrame:(CGRect)frame
                title:(NSString *)title
               action:(SEL)action
            addTarget:(id)addtarget
             forstate:(UIControlState)forstate
     forControlEvents:(UIControlEvents)forControlEvents
               myHang:(NSInteger)myHang;

@end
