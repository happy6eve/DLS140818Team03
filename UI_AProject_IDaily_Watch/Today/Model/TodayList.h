//
//  TodayList.h
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-15.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TodayList : NSObject
@property (nonatomic, retain)NSString *cover_thumb;
@property (nonatomic, retain)NSString *pubdate;
@property (nonatomic, retain)NSString *title;
@end
