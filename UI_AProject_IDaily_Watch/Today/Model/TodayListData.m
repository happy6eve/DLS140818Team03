//
//  TodayListData.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-15.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "TodayListData.h"
#import "AFHTTPRequestOperationManager.h"
#import "TodayList.h"
@implementation TodayListData
- (void)getDataFromBlock:(MyBlock)block
{
    
    
    self.dataBlock = block;
    self.dataAarrOne = [NSMutableArray array];
    AFHTTPRequestOperationManager *mananger1 = [AFHTTPRequestOperationManager manager];
    [mananger1 GET:@"http://watch-cdn.idailywatch.com/api/list/cover/zh-hans?page=1&ver=iphone&app_ver=9" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableArray *data = [NSMutableArray arrayWithArray:responseObject];
//        [data removeObjectAtIndex:5];
//        [data removeObjectAtIndex:0];
        for (NSDictionary *dic in data) {
            TodayList *list = [[TodayList alloc]init];
            list.title = [dic objectForKey:@"title"];
            list.pubdate = [dic  objectForKey:@"pubdate"];
            list.cover_thumb = [dic objectForKey:@"cover_thumb"];
            [_dataAarrOne addObject:list];
            
        }
         self.dataBlock(_dataAarrOne);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    
    
    
    //self.dataBlock(_datamodel);
    
}

@end
