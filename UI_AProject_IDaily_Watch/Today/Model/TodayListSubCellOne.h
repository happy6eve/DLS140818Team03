//
//  TodayListSubCellOne.h
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-16.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TodayListSubCellOne : NSObject
@property(nonatomic, retain)NSString *cover_thumb1;
@property(nonatomic, retain)NSString *cover_landscape;
@end
