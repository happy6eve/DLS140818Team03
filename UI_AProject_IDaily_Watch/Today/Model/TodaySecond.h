//
//  TodayListSub.h
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-16.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>
//Today listSub
@interface TodaySecond : NSObject
@property(nonatomic, retain)NSString *cover_landscape;
@property(nonatomic, retain)NSString *title;
@property(nonatomic, retain)NSString *source;
@property(nonatomic,retain)NSString *album;
@end
