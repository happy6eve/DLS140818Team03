//
//  MyButton.m
//  UI_代码库
//
//  Created by 安东 on 14-9-22.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "MyButton.h"

@implementation MyButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


+ (id)buttonWithFrame:(CGRect)frame
               title:(NSString *)title
              action:(SEL)action
            addTarget:(id)addtarget
             forstate:(UIControlState)forstate
     forControlEvents:(UIControlEvents)forControlEvents
myHang:(NSInteger)myHang
{
    MyButton *button = [MyButton buttonWithType:UIButtonTypeRoundedRect];
    button.myHang = myHang;
    button.frame = frame;
    [button setTitle:title forState:forstate];
    [button addTarget:addtarget action:action forControlEvents:forControlEvents];
    
    return [button autorelease];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
