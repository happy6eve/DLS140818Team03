//
//  AppDelegate.m
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-13.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "AppDelegate.h"
#import "TodayViewController.h"
#import "NewsViewController.h"
#import "BrandsViewController.h"
#import "VideoViewController.h"
#import "NewsNeViewController.h"
#import "NewsNe2ViewController.h"
#import "WikiViewController.h"
#import <ShareSDK/ShareSDK.h>
#import "Reachability.h"
@implementation AppDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.

    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    sleep(2);
    //判断当前为那种网络
    Reachability *reachability = [Reachability reachabilityWithHostName:@"www.apple.com"];
    switch ([reachability currentReachabilityStatus]) {
        case NotReachable:
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"当前无网络连接,无法加载数据!" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
            
            [alert show];
                        [alert release];
        }
            break;
        case ReachableViaWiFi:
            NSLog(@"wifi连接");
            break;
        case ReachableViaWWAN:
            NSLog(@"蜂窝数据");
            break;
        default:
            break;
    }

   
    
    
    
    
    
    //添加新浪微博应用 注册网址 http://open.weibo.com
    [ShareSDK registerApp:@"35205910da00"];//sdk
    [ShareSDK connectSinaWeiboWithAppKey:@"568898243"
                               appSecret:@"38a4f8204cc784f81f9f0daaf31e02e3"
                             redirectUri:@"http://www.sharesdk.cn"];
    

  
    

    

    [application setStatusBarStyle:UIStatusBarStyleLightContent];

    TodayViewController *today = [[TodayViewController alloc]init];
    
    UINavigationController * niv = [[UINavigationController alloc]initWithRootViewController:today];
   
    
    
    [self.window setRootViewController:niv];
    [today release];
    [niv release];

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
