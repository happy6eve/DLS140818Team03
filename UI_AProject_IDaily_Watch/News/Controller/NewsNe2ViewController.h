//
//  NewsNe2ViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-14.
//  Copyright (c) 2014年 蓝欧科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsNe2ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,retain)UITableView *table4;
@property(nonatomic ,assign)NSInteger count;
@property(nonatomic ,retain)NSMutableArray * dataSource;
@end
