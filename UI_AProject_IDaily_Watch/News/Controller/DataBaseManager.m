//
//  DataBaseManager.m
//  UI14_DataBase
//
//  Created by 安东 on 14-10-2.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "DataBaseManager.h"
#import "NewsModel.h"

static sqlite3 *dbPoint = nil;


@implementation DataBaseManager


+ (void)open
{
    if (dbPoint) {
        return;
    }
    //_mymodel = [[NewsModel alloc]init];
    
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    path = [path stringByAppendingPathComponent:@"Watch.rdb"];
    NSLog(@"%@", path);
   int result = sqlite3_open(path.UTF8String, &dbPoint);
    if (result == SQLITE_OK) {
        NSLog(@"数据库打开成功");
    }
}
+ (void)close
{
    sqlite3_close(dbPoint);
}
+ (void)create
{
    [self open];
    NSString *sql = @"create table watchyears (title text, pubdate1 text, image1 text, image2 text, summary text, content text)";
    sqlite3_exec(dbPoint, sql.UTF8String, NULL, NULL, NULL);
}
+ (void)insert:(NewsModel *)mymodel
{
    [self open];
    NSString *sql = [NSString stringWithFormat:@"insert into watchyears values ('%@','%@','%@','%@','%@','%@')", mymodel.title,mymodel.pubdate1,mymodel.image1,mymodel.image2,mymodel.summary,mymodel.content];
    sqlite3_exec(dbPoint, sql.UTF8String, NULL, NULL, NULL);
}
+ (void)remove:(NewsModel *)mymodel
{
    [self open];
    NSString *sql = [NSString stringWithFormat:@"delete from watchyears where title = '%@'",mymodel.title];
    sqlite3_exec(dbPoint, sql.UTF8String, NULL, NULL, NULL);
    
}
+ (void)update
{
    [self create];
    NSString *sql = @"update watchyears set name = '汲国兴250' where number = '250'";
    sqlite3_exec(dbPoint, sql.UTF8String, NULL, NULL, NULL);
    
}
+ (NSArray *)select
{
    [self open];
    //创建数据库替身
    sqlite3_stmt *stmt = nil;
    NSString *sql = @"select * from watchyears";
    //执行语句
   int result = sqlite3_prepare_v2(dbPoint, sql.UTF8String, -1, &stmt, NULL);
    //判断是否成功
    if (result == SQLITE_OK) {
        //创建数组用来保存查询的数据
        NSMutableArray *arr = [NSMutableArray array];
        //如果还有下一行
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            //获取数据
            const unsigned char * title = sqlite3_column_text(stmt, 0);
            const unsigned char * pubdate1 = sqlite3_column_text(stmt, 1);
            const unsigned char * image1 = sqlite3_column_text(stmt, 2);
            const unsigned char * image2 = sqlite3_column_text(stmt, 3);
            const unsigned char * summary = sqlite3_column_text(stmt, 4);
            const unsigned char * content = sqlite3_column_text(stmt, 5);
            //int number = sqlite3_column_int(stmt, 2);
            
            //封装成model
            NewsModel *mymodel = [[NewsModel alloc] init];
            mymodel.title = [NSString stringWithUTF8String:(const char *) title];
            mymodel.pubdate1 = [NSString stringWithUTF8String:(const char *) pubdate1];
            mymodel.image1 = [NSString stringWithUTF8String:(const char *) image1];
            mymodel.image2 = [NSString stringWithUTF8String:(const char *) image2];
            mymodel.summary = [NSString stringWithUTF8String:(const char *) summary];
            mymodel.content = [NSString stringWithUTF8String:(const char *) content];
            [arr addObject:mymodel];
            [mymodel release];
       }
        sqlite3_finalize(stmt);   //释放数据库替身
        return arr;
    }
    
    sqlite3_finalize(stmt);
    return [NSMutableArray array];
    
    
}


@end
