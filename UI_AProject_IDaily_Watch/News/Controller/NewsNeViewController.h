//
//  NewsNeViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-13.
//  Copyright (c) 2014年 蓝欧科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsModel.h"
#import "YRSideViewController.h"
@interface NewsNeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,retain)UITableView *table2;
@property(nonatomic ,retain)NSMutableArray * dataSource;
@property(nonatomic,assign)NSInteger count;
@property(nonatomic,assign)NSInteger countx;
@property(nonatomic,retain)NSMutableArray *arrayFile1;
@property(nonatomic,retain)NSMutableArray *arrayFile2;
@property(nonatomic,retain)NSMutableArray *arrayFile3;
@property(nonatomic,retain)NSMutableArray *arrayFile4;
@property(nonatomic,retain)NSString *path;
@property(nonatomic,retain)UIAlertView *alertShow;
@property(nonatomic,retain)NewsModel *model1;
@end
