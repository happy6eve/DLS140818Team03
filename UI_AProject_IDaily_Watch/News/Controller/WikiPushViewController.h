//
//  WikiPushViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-16.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WikiPushViewController : UIViewController
@property (nonatomic ,retain)UITableView * tableView;
@property (nonatomic ,retain)UILabel * label1;
@property (nonatomic ,retain)UILabel *label2;
@property (nonatomic ,retain)UIWebView * web;
@property (nonatomic ,retain)NSMutableArray * dataSource;
@property (nonatomic ,assign)NSInteger  index;
@end
