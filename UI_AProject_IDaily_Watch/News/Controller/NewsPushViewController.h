//
//  NewsPushViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-17.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsPushViewController : UIViewController<UITableViewDataSource,UITableViewDelegate, UIWebViewDelegate>
@property(nonatomic,retain)UITableView *table3;
@property(nonatomic,retain)UIWebView *webView;
@property(nonatomic,retain)NSMutableArray *getdata;
@property(nonatomic,assign)NSInteger index;
@end