//
//  DataBaseManager.h
//  UI14_DataBase
//
//  Created by 安东 on 14-10-2.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "NewsModel.h"
@interface DataBaseManager : NSObject


@property(nonatomic,retain)NewsModel *mymodel;


+ (void)open;
+ (void)create;
+ (void)insert:(NewsModel *)mymodel;
+ (void)remove:(NewsModel *)mymodel;
+ (void)update;
+ (NSArray *)select;

@end
