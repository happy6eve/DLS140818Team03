//
//  NewsPushViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-17.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "NewsPushViewController.h"
#import "NewsForthTableViewCell.h"
#import "NewsFifthTableViewCell.h"
#import "NewsModel.h"
#import "UIButton+WebCache.h"

NSInteger temp = 0;
@interface NewsPushViewController ()

@end

@implementation NewsPushViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _getdata = [[NSMutableArray alloc]init];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, Width * 60, Height * 30);
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回1.png"] forState:UIControlStateNormal];
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回2.png"] forState:UIControlStateHighlighted];
    [button setTitle:@"返回" forState:UIControlStateNormal];
    [button setTintColor:[UIColor whiteColor]];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button addTarget:self action:@selector(buttonAction1) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = item;
    
    
    
    
    // Do any additional setup after loading the view.
//    self.navigationController.navigationBar.translucent = NO;
//    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(80.0f, 0.0f, 160.0f, 44.0f)];
//    imageView.image = [UIImage imageNamed:@"TodayiDailyWatch.png"];
//    [self.navigationController.navigationBar addSubview:imageView];
    _table3 = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * (568 - 64)) style:UITableViewStylePlain];
    _table3.delegate = self;
    _table3.dataSource = self;
    self.table3.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_table3];
    self.tabBarController.tabBar.hidden = YES;
    
    
    
    
}
-(void)buttonAction1
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    [webView stringByEvaluatingJavaScriptFromString:
     @"var script = document.createElement('script');"
     "script.type = 'text/javascript';"
     "script.text = \"function ResizeImages() { "
     "var myimg,oldwidth;"
     "var maxwidth=320;"
     "for(i=0;i <document.images.length;i++){"
     "myimg = document.images;"
     "if(myimg.width > maxwidth){"
     "oldwidth = myimg.width;"
     "myimg.width = maxwidth;"
     "myimg.height = myimg.height * (maxwidth/oldwidth);"
     "}"
     "}"
     "}\";"
     "document.getElementsByTagName('head')[0].appendChild(script);"];
    [webView stringByEvaluatingJavaScriptFromString:@"ResizeImages();"];

    webView.frame = CGRectMake(0, 0, Width * 320,  _webView.scrollView.contentSize.height);
    [self.table3 reloadData];
    
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0 ) {
        NewsForthTableViewCell *cell = [[NewsForthTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
        cell.selectionStyle = NO;
        [cell.bbutton11 setBackgroundImageWithURL:[NSURL URLWithString:[_getdata[_index ]image2]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"1.png"]];
        [cell.blable1 setText:[NSString stringWithFormat:@"%@",[_getdata[_index ]title]]] ;
        
        [cell.blable1 setFont:[UIFont systemFontOfSize:13]];
        [cell.blable2 setText:[NSString stringWithFormat:@"%@",[_getdata[_index ]pubdate1]]] ;
        
        [cell.blable2 setFont:[UIFont systemFontOfSize:12]];
        
        [cell.blable4 setText:[NSString stringWithFormat:@"%@",[_getdata[_index ]summary]]] ;
        [cell.blable4 setFont:[UIFont systemFontOfSize:12]];
        
        return cell;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"web"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"web"];
        
        _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * 100)];
        _webView.scalesPageToFit = YES;
        _webView.scrollView.scrollEnabled = NO;
        _webView.delegate = self;
        [cell.contentView addSubview:_webView];
        NSString *str1 = [_getdata[_index ]content];
        NSString *str2 = @"<font size=30>";
        NSString *str3 = @"<font/>";
        NSString  *str = [NSString stringWithFormat:@"%@%@%@",str2,str1,str3];
       // NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:str]];
        _webView.backgroundColor = [UIColor orangeColor];
        NSLog(@"%d",temp++);
        [_webView loadHTMLString:str baseURL:nil];
    }
    
    
    return cell;
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return Height * 260;
    }
    return self.webView.scrollView.contentSize.height + 100;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
