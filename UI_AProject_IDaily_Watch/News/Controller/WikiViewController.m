//
//  WikiViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-16.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "WikiViewController.h"
#import "WikiTableViewCell.h"
#import "WikiData.h"
#import "WikiModel.h"
#import "WikiPushViewController.h"

@interface WikiViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation WikiViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        WikiData * data = [[WikiData alloc]init];
      
        [data getDataFromBlock:^(NSMutableArray* dataSource) {
            self.dataSource = dataSource;
             [_tableView reloadData];
           
        }];

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self createTableView];
    //[self loadWeather];
    // Do any additional setup after loading the view.
}
-(void)createTableView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0,Width * 320, Height * 568) style:UITableViewStyleGrouped];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_tableView];

}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    return _dataSource.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
    
    static NSString* indefault = @"wiki";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:indefault];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:indefault];
    }
           UIImageView * image = [[UIImageView alloc]initWithFrame:CGRectMake(50, 0, 320, 70)];
        image.image = [UIImage imageNamed:@"wiki.png"];
        [self.tableView addSubview:image];
    
    return cell;
    }
    if (indexPath.section == 1 ) {
        
        static NSString* indefault = @"wiki123";
        WikiTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:indefault];
        if (!cell) {
            cell = [[WikiTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:indefault];
        }
        cell.accessoryType = YES;
        cell.label.text = [_dataSource[indexPath.row]title];
        cell.label2.text = [_dataSource[indexPath.row]pubdate];
        return cell;
    }
    
    
    
    return 0;
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        
        
        
        
        WikiPushViewController * vc = [[WikiPushViewController alloc]init];
        vc.dataSource = _dataSource;
        vc.index = indexPath.row;
        
        [self.navigationController pushViewController:vc animated:YES];
        
        
        
        
    }
    
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 70;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
