//
//  WikiViewController.h
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-16.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WikiViewController : UIViewController
@property (nonatomic ,retain)UITableView * tableView;
@property(nonatomic ,retain)NSMutableArray * dataSource;
@property (retain, nonatomic) IBOutlet UIImageView *weathImage;
@property (retain, nonatomic) IBOutlet UILabel *weathText;
@property(nonatomic,retain)UIView *aView2;
@property(nonatomic,retain)UIButton *buttonContent1;
@property(nonatomic,retain)UILabel *lableweather1;
@property(nonatomic,retain)UILabel *lableweather2;
@property(nonatomic,retain)UILabel *lableweather3;
@end
