//
//  NewsNe2ViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-14.
//  Copyright (c) 2014年 蓝欧科技. All rights reserved.
//

#import "NewsNe2ViewController.h"
#import "NewsThirdTableViewCell.h"
#import "NewsPushViewController.h"
#import "News2Data.h"
#import "News3Data.h"
#import "News2Model.h"
#import "UIButton+WebCache.h"

@interface NewsNe2ViewController ()

@end

@implementation NewsNe2ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
      

        
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"%ld" , (long)_count);
  
    NSLog(@"%@", _dataSource);
    
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, Width * 60, Height * 30);
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回1.png"] forState:UIControlStateNormal];
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回2.png"] forState:UIControlStateHighlighted];
    [button setTitle:@"返回" forState:UIControlStateNormal];
    [button setTintColor:[UIColor whiteColor]];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = item;
    
    
    
//    self.navigationController.navigationBar.translucent = NO;
//    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(80.0f, 0.0f, 160.0f, 44.0f)];
//    imageView.image = [UIImage imageNamed:@"TodayiDailyWatch.png"];
//    [self.navigationController.navigationBar addSubview:imageView];

    // Do any additional setup after loading the view.
    _table4 = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * (568 - 64)) style:UITableViewStylePlain];
    _table4.delegate = self;
    _table4.dataSource = self;
     self.table4.backgroundColor = [UIColor colorWithRed:48 /255.0 green:48 / 255.0 blue:48 / 255.0 alpha:1];
    _table4.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched ;
    _table4.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview: _table4];

    
}
-(void)buttonAction
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
   

}
-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if (_dataSource.count == 1) {
        return _dataSource.count;
    }
    else
    return _dataSource.count - 1;
    

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NewsThirdTableViewCell *cell = [[NewsThirdTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    News2Model *model = _dataSource[indexPath.row];
    cell.selectionStyle = NO;
   [cell.sbutton2 setBackgroundImageWithURL:[NSURL URLWithString:model.image1] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"1.png"]];
    cell.alable2.text = model.title;
    cell.backgroundColor = [UIColor colorWithRed:48 /255.0 green:48 / 255.0 blue:48 / 255.0 alpha:1];
    cell.alable3.text = model.pubdate1;
    cell.alable4.text = model.summary;
    
    
    return cell;


}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{


    return 190;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    NewsPushViewController *vc = [[NewsPushViewController alloc]init];
    vc.getdata = _dataSource;
    vc.index = indexPath.row  ;
    
    
    
    [self.navigationController pushViewController:vc animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
