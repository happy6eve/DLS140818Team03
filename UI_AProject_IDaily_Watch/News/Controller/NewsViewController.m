//
//  NewsViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-13.
//  Copyright (c) 2014年 蓝欧科技. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsFirstTableViewCell.h"
#import "NewsNeViewController.h"
#import "NewsNe2ViewController.h"
#import "News2Data.h"
#import "News3Data.h"
#import "News2Model.h"
#import "News4Data.h"
#import "News5Data.h"

@interface NewsViewController ()

@end

@implementation NewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        NewsNe2ViewController * vc = [[NewsNe2ViewController alloc]init];
        News2Data * data2 = [[News2Data alloc]init];
        [data2 getDataFromBlock:^(NSMutableArray* dataSource) {
            self.dataSource = dataSource;
            [vc.table4 reloadData];
            
        }];
        News3Data * data3 = [[News3Data alloc]init];
        [data3 getDataFromBlock:^(NSMutableArray* dataSource) {
            self.dataSource2 = dataSource;
            [vc.table4 reloadData];
            
        }];
        News4Data * data4 = [[News4Data alloc]init];
        [data4 getDataFromBlock:^(NSMutableArray* dataSource) {
            self.dataSource3 = dataSource;
            [vc.table4 reloadData];
            
        }];
        News5Data * data5 = [[News5Data alloc]init];
        [data5 getDataFromBlock:^(NSMutableArray* dataSource) {
            self.dataSource4 = dataSource;
            [vc.table4 reloadData];
            
        }];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
       self.navigationController.navigationBar.translucent = NO;
    _table1 = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * 568 ) style:UITableViewStyleGrouped];
    self.table1.backgroundColor = [UIColor grayColor];
    _table1.delegate = self;
    _table1.dataSource = self;
    _table1.separatorStyle = UITableViewCellSeparatorStyleNone;
    _table1.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_table1];
    
    UIImageView * image = [[UIImageView alloc]initWithFrame:CGRectMake(Width * -80, 0, Width * 310, Height * 230)];
    image.image = [UIImage imageNamed:@"watch.png"];
    [self.table1 addSubview:image];
        
    
    
    
}

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return 4;


}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsFirstTableViewCell *cell = [[NewsFirstTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    cell.backgroundColor = [UIColor colorWithRed:48 /255.0 green:48 / 255.0 blue:48 / 255.0 alpha:1];
    
//    if (indexPath.row == 0) {
//        cell.lable1.text = @"NEWS";
//        cell.lable1.font = [UIFont systemFontOfSize:20];
//        cell.lable2.text = @"新闻";
//        cell.lable2.font = [UIFont systemFontOfSize:12];
//        cell.lable3.text = @"??";
//        cell.lable3.font = [UIFont systemFontOfSize:12];
//    
    cell.lable1.textColor = [UIColor whiteColor];
    cell.lable2.textColor = [UIColor whiteColor];
 
//    }
        if (indexPath.row == 0)
    {
    
        cell.lable1.text = @"COLUMN";
        cell.lable1.font = [UIFont systemFontOfSize:20];
        cell.lable2.text = @"专栏";
        cell.lable2.font = [UIFont systemFontOfSize:12];
        cell.lable3.text = @"??";
        cell.lable3.font = [UIFont systemFontOfSize:12];
        cell.selectionStyle = NO;
    }
    else if (indexPath.row == 1)
    {
        
        cell.lable1.text = @"OPINION";
        cell.lable1.font = [UIFont systemFontOfSize:20];
        cell.lable2.text = @"观点";
        cell.lable2.font = [UIFont systemFontOfSize:12];
        cell.lable3.text = @"??";
        cell.lable3.font = [UIFont systemFontOfSize:12];
        cell.selectionStyle = NO;
    }
    else if (indexPath.row == 2)
    {
        
        cell.lable1.text = @"FEATURE";
        cell.lable1.font = [UIFont systemFontOfSize:20];
        cell.lable2.text = @"深度";
        cell.lable2.font = [UIFont systemFontOfSize:12];
        cell.lable3.text = @"??";
        cell.lable3.font = [UIFont systemFontOfSize:12];
        cell.selectionStyle = NO;
    }
    else if (indexPath.row == 3)
    {
        
        cell.lable1.text = @"EVENTS";
        cell.lable1.font = [UIFont systemFontOfSize:20];
        cell.lable2.text = @"活动";
        cell.lable2.font = [UIFont systemFontOfSize:12];
        cell.lable3.text = @"??";
        cell.lable3.font = [UIFont systemFontOfSize:12];
        cell.selectionStyle = NO;
    }

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    return cell;
    


}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 225 * Height;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 70 * Height;


}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (indexPath.row == 0) {
        
 
        NewsNe2ViewController * vc1 = [[NewsNe2ViewController alloc]init];
       
        vc1.count = 1;
        vc1.dataSource = _dataSource;
        [self.navigationController pushViewController:vc1 animated:YES];

    }
    if (indexPath.row == 1) {
        NewsNe2ViewController *vc1 = [[NewsNe2ViewController alloc]init];
        
        vc1.count = 2;
        vc1.dataSource = _dataSource2;
        [self.navigationController pushViewController:vc1 animated:YES];
    }
    if (indexPath.row == 2) {
        NewsNe2ViewController *vc1 = [[NewsNe2ViewController alloc]init];
        
        vc1.count = 3;
        vc1.dataSource = _dataSource3;
        [self.navigationController pushViewController:vc1 animated:YES];
    }
    if (indexPath.row == 3) {
        NewsNe2ViewController *vc1 = [[NewsNe2ViewController alloc]init];
        
        vc1.count = 4;
        vc1.dataSource = _dataSource4;
        [self.navigationController pushViewController:vc1 animated:YES];
    }


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
