//
//  WikiPushViewController.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-16.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "WikiPushViewController.h"
#import "WikiModel.h"

@interface WikiPushViewController ()<UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate>

@end

@implementation WikiPushViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.dataSource = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
  

    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Width * 320, Height * 568) style:UITableViewStyleGrouped];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_tableView];
    [_tableView release];
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, Width * 60, Height * 30);
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回1.png"] forState:UIControlStateNormal];
//    [button setBackgroundImage:[UIImage imageNamed:@"TodayList返回2.png"] forState:UIControlStateHighlighted];
    [button setTitle:@"返回" forState:UIControlStateNormal];
    [button setTintColor:[UIColor whiteColor]];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = item;

    
    

    // Do any additional setup after loading the view.
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    webView.frame = CGRectMake(0,Height * 100, Width * 320, webView.scrollView.contentSize.height);
    [_tableView reloadData];
    
    
}
-(void)buttonAction
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
-(UITableViewCell * )tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    static NSString * indentifer = @"cell";
    UITableViewCell  * cell = [tableView dequeueReusableCellWithIdentifier:indentifer];
    if (!cell) {
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:indentifer]autorelease];
        
        
        _web = [[UIWebView alloc]initWithFrame:CGRectMake(0, Height * 110, Width * 320, Height * 200)];
        _web.scrollView.scrollEnabled = NO;
        _web.delegate = self;
        NSString * url = [_dataSource[_index]content];
        [_web loadHTMLString:url baseURL:nil];
        [self.tableView addSubview:_web];
    }
    
    cell.selectionStyle = NO;

    _label1 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 15, Height * 10, Width * 320, Height * 40)];
  
    _label1.text = [_dataSource[_index]title];
  
   // _label1.text = [_dataSource[indexPath.row]title];
    _label1.font = [UIFont systemFontOfSize:20];
    [self.tableView addSubview:_label1];
    
    _label2 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 190, Height * 60, Width * 130,  Height * 30)];
    _label2.text = [_dataSource[_index]pubdate];
    _label2.textColor = [UIColor lightGrayColor];
    [self.tableView addSubview:_label2];
    
    
    
    
    
    return cell;

    
    
    
    
    
    
    
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 100;
    }
    else
        return _web.scrollView.contentSize.height + 100;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
