//
//  News5Data.h
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-15.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^MyBlock) (NSMutableArray* dataSource);
@interface News5Data : NSObject



@property(nonatomic ,copy) MyBlock dataBlock;
@property(nonatomic ,retain)NSMutableArray * datamodel;

- (void)getDataFromBlock:(MyBlock)block;
@end
