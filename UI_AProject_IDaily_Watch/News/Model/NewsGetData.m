//
//  GetData.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-15.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "NewsGetData.h"
#import "AFHTTPRequestOperationManager.h"
#import "NewsModel.h"
@implementation NewsGetData
- (void)getDataFromBlock:(MyBlock)block
{
    
    
    self.dataBlock = block;
    self.datamodel = [NSMutableArray array];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://watch-cdn.idailywatch.com/api/list/magazine/zh-hans?page=1&ver=iphone&app_ver=9" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *arr = [NSArray array];
        arr = (NSArray *)responseObject;
        
        for (NSDictionary *dic in arr) {
           NewsModel  *mine = [[NewsModel alloc]init];
            mine.image1 = [dic objectForKey:@"cover_thumb"];
            mine.image2 = [dic objectForKey:@"cover_landscape_hd"];
            mine.title = [dic objectForKey:@"title"];
            mine.pubdate1 = [dic objectForKey:@"pubdate"];
            mine.summary = [dic objectForKey:@"summary"];
            mine.content = [dic objectForKey:@"content"];
            [_datamodel addObject:mine];
          
     
            
            
        }
        self.dataBlock(_datamodel);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
   

}

@end
