//
//  News2Data.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-15.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "News2Data.h"
#import "AFHTTPRequestOperationManager.h"
#import "News2Model.h"
@implementation News2Data
- (void)getDataFromBlock:(MyBlock)block
{
    
    
    self.dataBlock = block;
    self.datamodel = [NSMutableArray array];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://watch-cdn.idailywatch.com/api/list/column/zh-hans?page=1&ver=iphone&app_ver=9" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *arr = [NSArray array];
        arr = (NSArray *)responseObject;
        
        for (NSDictionary *dic in arr) {
            News2Model  *mine = [[News2Model alloc]init];
            mine.image1 = [dic objectForKey:@"cover_thumb"];
            mine.image2 = [dic objectForKey:@"cover_landscape_hd"];
            mine.title = [dic objectForKey:@"title"];
            mine.pubdate1 = [dic objectForKey:@"pubdate"];
            mine.summary = [dic objectForKey:@"summary"];
              mine.content = [dic objectForKey:@"content"];
            [_datamodel addObject:mine];
            
            
            
            
        }
        self.dataBlock(_datamodel);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    
}

@end
