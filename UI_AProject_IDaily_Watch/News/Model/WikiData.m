//
//  WikiData.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-16.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "WikiData.h"
#import "AFHTTPRequestOperationManager.h"
#import "WikiModel.h"
@implementation WikiData
- (void)getDataFromBlock:(MyBlock)block
{
    
    
    self.dataBlock = block;
    self.datamodel = [NSMutableArray array];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://watch-cdn.idailywatch.com/api/list/wiki/zh-hans?page=1&ver=iphone&app_ver=9" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *arr = [NSArray array];
        arr = (NSArray *)responseObject;
        
        for (NSDictionary *dic in arr) {
            WikiModel  *mine = [[WikiModel alloc]init];
            
            mine.title = [dic objectForKey:@"title"];
            mine.pubdate = [dic objectForKey:@"pubdate"];
            mine.content = [dic objectForKey:@"content"];
            [_datamodel addObject:mine];
            
            
            
            
        }
        self.dataBlock(_datamodel);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    
}

@end
