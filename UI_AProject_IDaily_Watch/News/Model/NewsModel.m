//
//  NewsModel.m
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-15.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "NewsModel.h"

@implementation NewsModel
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.image1 forKey:@"image1"];
    [aCoder encodeObject:self.image2 forKey:@"image2"];
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.pubdate1 forKey:@"pubdate1"];
    [aCoder encodeObject:self.summary forKey:@"summary"];
    [aCoder encodeObject:self.content forKey:@"content"];
    
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.image1 = [aDecoder decodeObjectForKey:@"image1"];
        self.image2 = [aDecoder decodeObjectForKey:@"image2"];
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.pubdate1 = [aDecoder decodeObjectForKey:@"pubdate1"];
        self.summary = [aDecoder decodeObjectForKey:@"summary"];
        self.content = [aDecoder decodeObjectForKey:@"content"];
    }
    return self;
}



@end
