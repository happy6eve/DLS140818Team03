//
//  NewsModel.h
//  UI_A段项目_iDaily Watch
//
//  Created by 徐继垚 on 14-10-15.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsModel : NSObject
@property(nonatomic ,retain)NSString * image1;
@property(nonatomic ,retain)NSString * image2;
@property(nonatomic ,retain)NSString * title;
@property(nonatomic ,retain)NSString * pubdate1;
@property(nonatomic ,retain)NSString * summary;
@property(nonatomic ,retain)NSString * content;

@end
