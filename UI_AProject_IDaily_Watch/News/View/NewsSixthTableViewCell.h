//
//  NewsSixthTableViewCell.h
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-16.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsSixthTableViewCell : UITableViewCell
@property(nonatomic,retain)UIButton *dbutton1;
@property(nonatomic,retain)UILabel *dlable1;
@property(nonatomic,retain)UILabel *dlable2;
@property(nonatomic,retain)UILabel *dlable3;
@property(nonatomic,retain)UILabel *dlable4;
@end
