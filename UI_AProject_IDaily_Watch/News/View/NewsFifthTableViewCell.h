//
//  NewsFifthTableViewCell.h
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-13.
//  Copyright (c) 2014年 蓝欧科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsFifthTableViewCell : UITableViewCell
@property(nonatomic,retain)UIWebView *webView;
@end
