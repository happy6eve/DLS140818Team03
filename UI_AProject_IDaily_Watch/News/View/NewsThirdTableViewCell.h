//
//  NewsThirdTableViewCell.h
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-13.
//  Copyright (c) 2014年 蓝欧科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsThirdTableViewCell : UITableViewCell
@property(nonatomic,retain)UIButton *sbutton2;
@property(nonatomic,retain)UILabel *alable2;
@property(nonatomic,retain)UILabel *alable3;
@property(nonatomic,retain)UILabel *alable4;
@property(nonatomic,retain)UIButton *button5;
@property(nonatomic,retain)NSString *imageTitle;
@end
