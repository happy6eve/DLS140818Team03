//
//  NewsForthTableViewCell.m
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-13.
//  Copyright (c) 2014年 蓝欧科技. All rights reserved.
//

#import "NewsForthTableViewCell.h"

@implementation NewsForthTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _bbutton11 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _bbutton11.backgroundColor = [UIColor cyanColor];
        _bbutton11.frame = CGRectMake(0, 0,Width * 320,Height * 160);
        [self.contentView addSubview:_bbutton11];
        _blable1 = [[UILabel alloc]initWithFrame:CGRectMake(0,Height * 170,Width * 320,Height * 30)];
        _blable1.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_blable1];
        _blable2 = [[UILabel alloc]initWithFrame:CGRectMake(0,Height * 205,Width * 320,Height * 30)];
        _blable2.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_blable2];
//        _blable3 = [[UILabel alloc]initWithFrame:CGRectMake(200, 205, 100, 15)];
//        _blable3.backgroundColor = [UIColor greenColor];
        //[self.contentView addSubview:_blable3];
        _blable4 = [[UILabel alloc]initWithFrame:CGRectMake(0,Height * 235,Width * 320,Height * 20)];
        _blable4.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_blable4];
        
        
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
