//
//  NewsThirdTableViewCell.m
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-13.
//  Copyright (c) 2014年 蓝欧科技. All rights reserved.
//

#import "NewsThirdTableViewCell.h"

@implementation NewsThirdTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        _sbutton2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _sbutton2.frame = CGRectMake( 30, 30, 100, 100);
        _sbutton2.layer.shadowColor = [UIColor whiteColor].CGColor;
        _sbutton2.layer.shadowOpacity = 6;
        _sbutton2.backgroundColor = [UIColor orangeColor];
        [self.contentView addSubview:_sbutton2];
        
        _alable2 = [[UILabel alloc]initWithFrame:CGRectMake(155, 10, 165, 70)];
        //_alable2.backgroundColor = [UIColor grayColor];
        _alable2.numberOfLines = 0;
        _alable2.textColor = [UIColor whiteColor];
        _alable2.text =@"『新表』Longines 推出潜水表：Heritage Diver 登场";

        _alable2.font =[UIFont systemFontOfSize:13];
        [self.contentView addSubview:_alable2];
        
        _alable3 = [[UILabel alloc]initWithFrame:CGRectMake(155, 80, 135, 15)];
        //_alable3.backgroundColor = [UIColor greenColor];
        _alable3.numberOfLines = 0;
        _alable3.textColor = [UIColor whiteColor];
        _alable3.font =[UIFont systemFontOfSize:13];
        [self.contentView addSubview:_alable3];
        
        _button5 = [[UIButton alloc]initWithFrame:CGRectMake(125, 100, 135, 30)];
        //_alable3.backgroundColor = [UIColor greenColor];
        _button5.titleLabel.textAlignment = NSTextAlignmentLeft;
        [_button5 setTitle:@"收藏            ❤️" forState:UIControlStateNormal];
        
        _button5.font =[UIFont systemFontOfSize:11];
        [self.contentView addSubview:_button5];
        
        _alable4 = [[UILabel alloc]initWithFrame:CGRectMake(30, 145, 280, 40)];
        //_alable4.backgroundColor = [UIColor greenColor];
        _alable4.numberOfLines = 0;
        _alable4.textColor = [UIColor whiteColor];
        _alable4.font =[UIFont systemFontOfSize:11];
        [self.contentView addSubview:_alable4];
        
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
