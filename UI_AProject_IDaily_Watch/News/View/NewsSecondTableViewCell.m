//
//  NewsSecondTableViewCell.m
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-13.
//  Copyright (c) 2014年 蓝欧科技. All rights reserved.
//

#import "NewsSecondTableViewCell.h"

@implementation NewsSecondTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
      
        _bbutton1 = [[UIButton alloc]initWithFrame:CGRectMake(0, 0,Width *  320,Height * 150)];
        _bbutton1.backgroundColor = [UIColor cyanColor];
        [self.contentView addSubview:_bbutton1];
        _alable1 = [[UILabel alloc]initWithFrame:CGRectMake(0,Height * 120,Width * 320,Height * 30)];
        _alable1.backgroundColor = [UIColor blackColor];
        _alable1.textColor = [UIColor whiteColor];
        _alable1.font = [UIFont systemFontOfSize:16];
        _alable1.alpha = 0.6;
        [self.contentView addSubview:_alable1];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
