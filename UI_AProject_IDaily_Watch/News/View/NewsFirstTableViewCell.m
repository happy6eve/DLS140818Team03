//
//  NewsFirstTableViewCell.m
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-13.
//  Copyright (c) 2014年 蓝欧科技. All rights reserved.
//

#import "NewsFirstTableViewCell.h"

@implementation NewsFirstTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        
        _lable1 = [[UILabel alloc]initWithFrame:CGRectMake(20, 5, 100, 30)];
        _lable1.backgroundColor = [UIColor clearColor];
        _lable1.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_lable1];
        
        
        _lable2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 40, 30, 20)];
        _lable2.backgroundColor = [UIColor clearColor];
               _lable2.font = [UIFont systemFontOfSize:12];
        _lable2.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_lable2];
        
        
        _lable3 = [[UILabel alloc]initWithFrame:CGRectMake(230, 20, 50, 28)];
        _lable3.backgroundColor = [UIColor whiteColor];
        _lable3.textAlignment = NSTextAlignmentLeft;
        _lable3.layer.cornerRadius = 15;
        _lable3.layer.masksToBounds = YES;
        _lable3.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:_lable3];
        
        _lable4 = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 2, 55)];
        _lable4.backgroundColor = [UIColor whiteColor];
        _lable4.textAlignment = NSTextAlignmentLeft;
        _lable4.font = [UIFont systemFontOfSize:20];
        [self.contentView addSubview:_lable4];
        
    
        
        
        
        
        
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
