//
//  NewsFifthTableViewCell.m
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-13.
//  Copyright (c) 2014年 蓝欧科技. All rights reserved.
//

#import "NewsFifthTableViewCell.h"

@implementation NewsFifthTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 5, 320, 600)];
        
        _webView.scalesPageToFit = YES;
        [self.contentView addSubview:_webView];
        NSString  *str = [NSString stringWithFormat:@"最近，很多品牌都开始为旗下的腕表系列推出X0周年款式，对于计时品牌来说，周年庆祝不仅是对自身历史的证明，也是对制表工艺突破上世纪的困难，延续发展至今的一种庆祝。<br />\r\n<br />\r\nChopard 的 Happy Sport 便是钻石工艺腕表系列里最经典系列之一，第一款 Happy Sport 发布于1993年，设计者是 Chopard 现任艺术总监和联合总裁 Caroline Scheufele，当时凭着一句“自由的钻石更快乐”，珠宝艺人们用将钻石包嵌在金圈中，在表盘上创造出全新的华丽空间。<br />\r\n<br />\r\n今年这款腕表20周年，Chopard 的庆祝方式是一款满钻镶嵌的超奢华限量款，在18K白金表壳和表带上，镶嵌了958颗长梯形切割的钻石，和1978颗圆形闪亮切割的小钻，整块表用钻达到65克拉，价格也独一无二————150万瑞郎。<br />\r\n<a href=\"http://pic.yupoo.com/fotomag/CRDTesOq/5pAlE.jpg\" target=\"_blank\" class=\"l \"><img src=\"http://pic.yupoo.com/fotomag/CRDTesOq/5pAlE.jpg\" class=\"noBorder\" alt=\"\" /></a><br />\r\n表壳直径 43mm，表镜和表背都是防眩光的蓝宝石水晶材质，表圈和表带都被精密切割的钻石布满，这些钻石的切割制作即需要4500小时，镶嵌则需要1700小时，整块表看上去就像是钻石排列而成，18K白金被藏在钻石光之下。<br />\r\n<br />\r\n镶钻白色表盘搭配18K白金小时标记，用大写罗马数字标识出3点和12点位置，剑形时分针则是镀铑的白金材质。<br />\r\n<br />\r\n搭载 Chopard 自家的 L.U.C 96.17-L 手动上链机芯，动力存储达到65小时，机芯摆频 28,800，机芯厚度仅有3.3mm。<br />\r\n<a href=\"http://pic.yupoo.com/fotomag/CRDTgH1K/vQdfN.jpg\" target=\"_blank\" class=\"l \"><img src=\"http://pic.yupoo.com/fotomag/CRDTgH1K/vQdfN.jpg\" "];
       // NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:str]];
        _webView.backgroundColor = [UIColor orangeColor];
        
        [_webView loadHTMLString:str baseURL:nil];
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
