//
//  NewsSixthTableViewCell.m
//  UI_A段项目_iDaily Watch
//
//  Created by lianglide on 14-10-16.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "NewsSixthTableViewCell.h"

@implementation NewsSixthTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        _dbutton1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _dbutton1.frame = CGRectMake(10, 10, Width * 150,Height * 150);
        _dbutton1.backgroundColor = [UIColor blackColor];
        _dbutton1.layer.shadowColor = [UIColor blackColor].CGColor;
        _dbutton1.layer.shadowOpacity = 3;
        [self.contentView addSubview:_dbutton1];
        _dlable1 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 165, Height * 10, Width * 150,Height * 40)];
        _dlable1.backgroundColor = [UIColor clearColor];
        _dlable1.textColor = [UIColor whiteColor];
        [self.contentView addSubview:_dlable1];
        _dlable2 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 165, Height * 55,Width * 150,Height * 20)];
        _dlable2.backgroundColor = [UIColor clearColor];
        _dlable2.textColor = [UIColor whiteColor];
        [self.contentView addSubview:_dlable2];
        
        _dlable3 = [[UILabel alloc]initWithFrame:CGRectMake(Width * 165,Height * 80,Width * 150,Height * 20)];
        _dlable3.backgroundColor = [UIColor clearColor];
        _dlable3.textColor = [UIColor whiteColor];
        [self.contentView addSubview:_dlable3];
    
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
