//
//  AppDelegate.h
//  UI_A段项目_iDaily Watch
//
//  Created by 刘旭 on 14-10-13.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YRSideViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) YRSideViewController *sideViewController;
@property (nonatomic, retain)UIImage *pic;
@end
